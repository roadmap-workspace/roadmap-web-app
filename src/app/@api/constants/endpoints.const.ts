export const endpoints = {
  // LookUp
  workDomains: 'work-domains',
  plans: 'plans',

  // Auth
  signup: 'auth/comp/local/signup',
  signin: 'auth/comp/local/signin',
  refresh: 'auth/comp/refresh',
  singout: 'auth/comp/logout',

  // Department
  departments: 'departments',

  // Roadmap
  roadmaps: 'roadmaps',

  // Node
  nodes: 'nodes',

  // QB
  questions: 'questions',

  // Feedback
  feedbacks: 'feedbacks',

  // Statistics
  statistics: 'statistics/companies',

  // Profile
  profile: 'company/profile',
};
