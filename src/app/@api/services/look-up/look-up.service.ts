import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { endpoints } from '../../constants/endpoints.const';
import { Feedback } from '../../models/feedback.interface';
import { Plan } from '../../models/plan.interface';
import { WorkDomain } from '../../models/work-domain.interface';

@Injectable({
  providedIn: 'root',
})
export class LookUpService {
  domainEndpoint: string;
  planEndpoint: string;
  feedbackEndpoint: string;
  constructor(private http: HttpClient) {
    this.domainEndpoint = `${environment.baseURL}/${endpoints.workDomains}`;
    this.planEndpoint = `${environment.baseURL}/${endpoints.plans}`;
    this.feedbackEndpoint = `${environment.baseURL}/${endpoints.feedbacks}`;
  }

  // *Work Domains*
  postWorkDomain(text: string) {
    return this.http.post<WorkDomain>(this.domainEndpoint, text);
  }

  getWorkDomains(): Observable<WorkDomain[]> {
    return this.http.get<WorkDomain[]>(this.domainEndpoint);
  }

  getWorkDomain(id: string): Observable<WorkDomain> {
    return this.http.get<WorkDomain>(`${this.domainEndpoint}/${id}`);
  }

  patchWorkDomain(id: string, text: string) {
    return this.http.patch<WorkDomain>(`${this.domainEndpoint}/${id}`, text);
  }

  deleteWorkDomain(id: string) {
    return this.http.delete<WorkDomain>(`${this.domainEndpoint}/${id}`);
  }

  // *Plans*
  postPlan(text: string) {
    return this.http.post<Plan>(this.planEndpoint, text);
  }

  getPlans(): Observable<Plan[]> {
    return this.http.get<Plan[]>(this.planEndpoint);
  }

  getPlan(id: string): Observable<Plan> {
    return this.http.get<Plan>(`${this.planEndpoint}/${id}`);
  }

  patchPlan(id: string, text: string) {
    return this.http.patch<Plan>(`${this.planEndpoint}/${id}`, text);
  }

  deletePlan(id: string) {
    return this.http.delete<Plan>(`${this.planEndpoint}/${id}`);
  }

  postFeedback(feedback: Feedback): Observable<Feedback> {
    return this.http.post<Feedback>(`${this.feedbackEndpoint}`, feedback);
  }
}
