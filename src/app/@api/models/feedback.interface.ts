export interface Feedback {
  email: string;
  fullName: string;
  message: string;
}
