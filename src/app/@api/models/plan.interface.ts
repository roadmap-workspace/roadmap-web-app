export interface Plan {
  _id: number;
  name: string;
  roadMapsCount: number;
  jobOpeningsCount: number;
  pontentialsRowsCount: number;
}
