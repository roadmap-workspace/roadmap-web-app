/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { GoogleOneTabService } from 'src/app/@core/services/google-one-tab/google-one-tab.service';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { Company } from '../../models/company.interface';
import { AuthService } from '../../services/auth.service';
// eslint-disable-next-line no-var
const options = {
  client_id:
    '426784544209-uok4l3nf27stigrbbei6dq54c2r62hmn.apps.googleusercontent.com', // required
  auto_select: false, // optional
  cancel_on_tap_outside: false, // optional
  context: 'signin', // optional
};
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  hide: boolean;
  submitted: boolean;
  isLoaded: boolean;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router,
    private googleOneTabService: GoogleOneTabService
  ) {
    if (this.authService.isAuth()) {
      router.navigate(['/panel/dashboard']);
    }
    this.hide = true;
    this.submitted = false;
    this.login = this.initLogin();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.login.controls;
  }

  ngOnInit(): void {
    this.googleOneTabService.signinWithGoogle((response) => {
      console.warn(response);
    });
  }

  initLogin(): FormGroup {
    return this.fb.group({
      email: [
        '',
        [
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$'),
        ],
      ],
      password: ['', Validators.required],
    });
  }

  submit(): void {
    this.submitted = true;
    if (this.login.invalid) {
      return;
    }
    this.authService.signin(this.login.value).subscribe(
      (company: Company) => {
        this.storageService.setLocalObject('company', company);
        this.storageService.setToken(company.accessToken);
        this.storageService.setRefreshToken(company.refreshToken);
        this.router.navigate(['/panel/dashboard']);
      },
      (err) => {
        console.error(err);
      }
    );
  }
}
