import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Plan } from 'src/app/@api/models/plan.interface';
import { WorkDomain } from 'src/app/@api/models/work-domain.interface';
import { LookUpService } from 'src/app/@api/services/look-up/look-up.service';
import { FormDataService } from 'src/app/@core/services/form-data/form-data.service';
import { ScreenService } from 'src/app/@core/services/screen/screen.service';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { SnackbarService } from 'src/app/@material/services/snackbar.service';
import { Company } from '../../models/company.interface';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent implements OnInit, OnDestroy {
  // Subscriptions
  screenSubscription: Subscription;
  workDomains$: Observable<WorkDomain[]>;
  /* plans$: Observable<Plan[]>; */
  // States
  isBelowLg: boolean;
  hide: boolean;
  isLoaded: boolean;
  // Look-Ups
  workDomains: WorkDomain[];
  plans: Plan[];
  // Utility Members
  selectedPlan: number;
  startTime: string;
  endTime: string;
  // FormGroups
  info: FormGroup;
  credentials: FormGroup;
  domain: FormGroup;
  /* plan: FormGroup; */
  optional: FormGroup;
  // FormGroup States
  infoSubmitted: boolean;
  credentialsSubmitted: boolean;
  workDomainSubmitted: boolean;
  /* planSubmitted: boolean; */
  optionalSubmitted: boolean;

  // Image Files & States
  isLogoLoading: boolean;
  isCoverLoading: boolean;
  logo: File;
  coverImage: File;

  constructor(
    private screenService: ScreenService,
    private fb: FormBuilder,
    private lookUpService: LookUpService,
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router,
    private formDataSerivce: FormDataService,
    private snackBarService: SnackbarService
  ) {
    this.isBelowLg = false;
    this.hide = true;
    this.isLoaded = false;
    this.workDomains = [];
    this.plans = [];
    this.selectedPlan = -1;
    this.startTime = '';
    this.endTime = '';
    this.infoSubmitted = false;
    this.credentialsSubmitted = false;
    this.workDomainSubmitted = false;
    /* this.planSubmitted = false; */
    this.optionalSubmitted = false;
    this.isLogoLoading = false;
    this.isCoverLoading = false;
    this.info = this.initInfo();
    this.credentials = this.initCredentials();
    this.domain = this.initWorkDomain();
    /* this.plan = this.initPlan(); */
    this.optional = this.initOptional();
  }

  //#region FormGroup Controls Getters
  get infoCtrls(): { [key: string]: AbstractControl } {
    return this.info.controls;
  }

  get credentialsCtrls(): { [key: string]: AbstractControl } {
    return this.credentials.controls;
  }

  get workDomainCtrls(): { [key: string]: AbstractControl } {
    return this.domain.controls;
  }

  /* get planCtrls(): { [key: string]: AbstractControl } {
    return this.plan.controls;
  } */

  get optionalCtrls(): { [key: string]: AbstractControl } {
    return this.optional.controls;
  }
  //#endregion

  //#region FormGroup Initializers
  initInfo(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      logo: [''],
      coverImage: [''],
    });
  }

  initCredentials(): FormGroup {
    return this.fb.group({
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$/),
        ],
      ],
      password: ['', Validators.required],
    });
  }

  initWorkDomain(): FormGroup {
    return this.fb.group({
      workDomain: this.fb.group({
        _id: ['', Validators.required],
        text: ['', Validators.required],
      }),
    });
  }

  /* initPlan(): FormGroup {
    return this.fb.group({
      plan: this.fb.group({
        _id: ['', Validators.required],
        name: ['', Validators.required],
        roadMapsCount: ['', Validators.required],
        jobOpeningsCount: ['', Validators.required],
        pontentialsRowsCount: ['', Validators.required],
      }),
    });
  } */

  initOptional(): FormGroup {
    return this.fb.group({
      website: [''],
      about: [''],
      workHours: [''],
      timeZone: [
        '',
        Validators.pattern(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/),
      ],
      numOfEmployees: ['', [Validators.min(1)]],
    });
  }
  //#endregion

  ngOnInit() {
    this.subscribeScreen();
    this.getWorkDomains();
    this.getPlans();
  }

  //#region Subscriptions
  subscribeScreen() {
    this.screenSubscription = this.screenService
      .isBelowLg()
      .subscribe((isBelowLg) => {
        this.isBelowLg = isBelowLg.matches;
      });
  }

  getWorkDomains() {
    this.workDomains$ = this.lookUpService.getWorkDomains();
  }

  getPlans() {
    this.lookUpService.getPlans().subscribe((res) => {
      this.plans = res;
      console.warn(this.plans);
    });
  }
  //#endregion

  /* selectPlan(plan: Plan) {
    this.plan = this.fb.group({
      plan: [
        {
          _id: plan._id,
          name: plan.name,
          roadMapsCount: plan.roadMapsCount,
          jobOpeningsCount: plan.jobOpeningsCount,
          pontentialsRowsCount: plan.pontentialsRowsCount,
        },
        [Validators.required],
      ],
    });
  } */

  selectWorkDomain(workDomain: WorkDomain) {
    this.workDomainSubmitted = true;
    this.domain = this.fb.group({
      workDomain: [
        {
          _id: workDomain._id,
          text: workDomain.text,
        },
        [Validators.required],
      ],
    });
  }

  createAccount() {
    this.optionalSubmitted = true;
    this.optional.patchValue({
      workHours: `${this.startTime} - ${this.endTime}`,
    });
    if (this.optional.invalid) {
      return;
    }
    this.signup();
  }

  skipAndCreateAccount() {
    this.setOptionalFormEmpty();
    this.signup();
  }

  setOptionalFormEmpty() {
    this.optional.setValue({
      website: '',
      about: '',
      workHours: '',
      timeZone: '',
      numOfEmployees: '',
    });
  }

  signup() {
    delete this.info.value.logo;
    delete this.info.value.coverImage;
    const companyFormData = {
      ...this.info.value,
      ...this.credentials.value,
      ...this.domain.value,
      ...this.optional.value,
      plan: this.plans[0],
    };
    const formData = this.formDataSerivce.jsonToFormData(companyFormData);
    formData.append('logo', this.logo);
    formData.append('coverImage', this.coverImage);
    this.authService.signup(formData).subscribe(
      (company: Company) => {
        this.storageService.setLocalObject('company', company);
        this.storageService.setToken(company.accessToken);
        this.storageService.setRefreshToken(company.refreshToken);
        this.router.navigate(['/panel/dashboard']);
      },
      (err) => {
        console.error(err);
      }
    );
  }

  uploadLogo(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target.files) {
      const file = target.files[0];
      const pattern = /image-*/;
      if (!file.type.match(pattern)) {
        this.snackBarService.openSnackBar(
          'toast.notSupported',
          'privacy_tip',
          3000
        );
        return;
      }
      this.isLogoLoading = true;
      const reader = new FileReader();
      reader.onload = (e) => {
        this.infoCtrls.logo.setValue(reader.result);
        this.isLogoLoading = false;
        this.logo = file;
      };
      reader.readAsDataURL(file);
    }
  }

  deleteLogo() {
    this.infoCtrls.logo.setValue('');
    this.logo = null;
  }

  uploadCoverImage(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target.files) {
      const file = target.files[0];
      const pattern = /image-*/;
      if (!file.type.match(pattern)) {
        this.snackBarService.openSnackBar(
          'toast.notSupported',
          'privacy_tip',
          3000
        );
        return;
      }
      this.isCoverLoading = true;
      const reader = new FileReader();
      reader.onload = (e) => {
        this.infoCtrls.coverImage.setValue(reader.result);
        this.isCoverLoading = false;
        this.coverImage = file;
      };
      reader.readAsDataURL(file);
    }
  }

  deleteCoverImage() {
    this.infoCtrls.coverImage.setValue('');
    this.coverImage = null;
  }

  ngOnDestroy(): void {
    this.screenSubscription.unsubscribe();
  }
}
