import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './components/login/login.component';
import { IonicModule } from '@ionic/angular';
import { TuiModule } from '../@tui/tui.module';
import { MaterialModule } from '../@material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../@shared/shared.module';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { StepperComponent } from './components/stepper/stepper.component';

@NgModule({
  declarations: [LoginComponent, SignUpComponent, StepperComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    IonicModule,
    TuiModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
  ],
})
export class AuthModule {}
