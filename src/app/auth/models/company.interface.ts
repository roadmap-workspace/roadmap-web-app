import { Plan } from 'src/app/@api/models/plan.interface';
import { WorkDomain } from 'src/app/@api/models/work-domain.interface';

export interface Company {
  name: string;
  email: string;
  password: string;
  workDomain: WorkDomain;
  plan: Plan;
  website: URL;
  about: string;
  workHours: string;
  timeZone: string;
  numOfEmployees: number;
  accessToken: string;
  refreshToken: string;
  logo: string;
  coverImage: string;
}
