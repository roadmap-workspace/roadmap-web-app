import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { endpoints } from 'src/app/@api/constants/endpoints.const';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { environment } from 'src/environments/environment';
import { Company } from '../models/company.interface';
import { Credentials } from '../models/credentials.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private storageService: StorageService,
    private router: Router
  ) {}

  signin(credentials: Credentials): Observable<Company> {
    return this.http.post<Company>(
      `${environment.baseURL}/${endpoints.signin}`,
      credentials
    );
  }

  signup(company: FormData): Observable<Company> {
    return this.http.post<Company>(
      `${environment.baseURL}/${endpoints.signup}`,
      company
    );
  }

  refreshToken(): Observable<any> {
    const refreshToken = this.storageService.getRefreshToken();
    const headers = new HttpHeaders().set(
      'authorization',
      `Bearer ${refreshToken}`
    );
    return this.http.post<any>(
      `${environment.baseURL}/${endpoints.refresh}`,
      {},
      { headers }
    );
  }

  removeCredentials() {
    this.storageService.removeLocalObject('company');
    this.storageService.removeToken();
    this.storageService.removeRefreshToken();
    this.router.navigate(['/auth/login']);
  }

  signout() {
    return this.http
      .post(`${environment.baseURL}/${endpoints.singout}`, {})
      .subscribe(() => {});
  }

  isAuth(): boolean {
    return this.storageService.getToken() ? true : false;
  }

  checkExpiry(): boolean {
    try {
      const splittedToken = this.storageService.getToken().split('.')[1];
      const expiry = JSON.parse(atob(splittedToken)).exp;
      return Math.floor(new Date().getTime() / 1000) < expiry;
    } catch (err) {}
    return false;
  }
}
