import { Direction } from '@angular/cdk/bidi';

export interface Language {
  name: string;
  dir: Direction;
}
