import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpErrorResponse,
  HttpStatusCode,
  HttpResponse,
  HttpEvent,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, switchMap, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/services/auth.service';
import { StorageService } from '../../services/storage/storage.service';
import { endpoints } from 'src/app/@api/constants/endpoints.const';
import { SnackbarService } from 'src/app/@material/services/snackbar.service';
@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  constructor(
    private snackBarService: SnackbarService,
    private storageService: StorageService,
    private authService: AuthService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {
        if (
          !request.url.includes('translation') &&
          event instanceof HttpResponse
        ) {
          switch (event.status) {
            case HttpStatusCode.Created:
              this.snackBarService.openSnackBar('toast.created', 'draw', 3000);
              break;
          }
        }
      }),
      //TODO: Reactivate the following line on production
      // -> retry(1),
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof ErrorEvent) {
          console.error(error);
          // handle client-side Response errors
        } else {
          // handle server-side Response errors
          switch (error.status) {
            case 0:
              this.snackBarService.openSnackBar(
                'toast.unknown',
                'signal_wifi_statusbar_connected_no_internet_4',
                2000
              );
              break;
            case HttpStatusCode.BadRequest:
              this.snackBarService.openSnackBar(
                error.error.message,
                'report',
                3000
              );
              break;
            case HttpStatusCode.Unauthorized:
              if (!request.url.includes(endpoints.refresh)) {
                return this.authService.refreshToken().pipe(
                  switchMap((tokens: any) => {
                    this.storageService.setToken(tokens.accessToken);
                    this.storageService.setRefreshToken(tokens.refreshToken);
                    this.snackBarService.openSnackBar(
                      'toast.refresh',
                      'cached',
                      3000
                    );
                    return next.handle(
                      request.clone({
                        headers: request.headers.set(
                          'authorization',
                          `Bearer ${tokens.accessToken}`
                        ),
                      })
                    );
                  }),
                  catchError((err) => {
                    this.authService.removeCredentials();
                    return throwError(err);
                  })
                );
              } else {
                this.authService.removeCredentials();
              }
              break;
            case HttpStatusCode.Forbidden:
              break;
            case HttpStatusCode.NotFound:
              if (request.url.includes(endpoints.signin)) {
                this.snackBarService.openSnackBar(
                  'toast.checkCredentials',
                  'fingerprint',
                  3000
                );
              }
              break;
            case HttpStatusCode.Conflict:
              this.snackBarService.openSnackBar(
                'toast.alreadyExist',
                'Responseerror',
                3000
              );
              break;
            case HttpStatusCode.InternalServerError:
              break;
            default:
              break;
          }
        }
        return throwError(error);
      })
    );
  }
}
