import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { StorageService } from '../../services/storage/storage.service';
import { endpoints } from 'src/app/@api/constants/endpoints.const';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private storageService: StorageService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const token: string = this.storageService.getToken() || '';
    let headers: HttpHeaders = request.headers;
    let clonedRequest = request.clone({ headers });
    if (!request.url.includes(endpoints.refresh)) {
      if (token) {
        headers = request.headers.set('authorization', `Bearer ${token}`);
        clonedRequest = request.clone({ headers });
      }
    }
    return next.handle(clonedRequest);
  }
}
