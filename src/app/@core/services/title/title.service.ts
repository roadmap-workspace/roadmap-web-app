import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TitleService {
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private title: Title
  ) {}

  changeTitle(): void {
    const tabTitle = this.title.getTitle();
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => {
          let child = this.activatedRoute.firstChild;
          if (child) {
            while (child.firstChild) {
              child = child.firstChild;
            }
            if (child.snapshot.data.title) {
              return child.snapshot.data.title;
            }
            return tabTitle;
          }
        })
      )
      .subscribe((title: string) => {
        this.title.setTitle(title);
      });
  }
}
