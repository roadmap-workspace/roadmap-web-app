import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FormDataService {
  constructor() {}
  jsonToFormData(data: any) {
    const formData = new FormData();
    this.buildFormData(formData, data);
    return formData;
  }

  private buildFormData(formData: FormData, data: any) {
    Object.keys(data).forEach((key) => {
      if (this.isTraversable(key)) {
        formData.append(key, data[key]);
      } else {
        formData.append(key, JSON.stringify(data[key]));
      }
    });
  }

  private isTraversable(key: any): boolean {
    return key && typeof key === 'object';
  }
}
