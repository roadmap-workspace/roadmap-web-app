import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  removeToken() {
    localStorage.removeItem('token');
  }

  setRefreshToken(refreshtoken: string) {
    localStorage.setItem('refreshtoken', refreshtoken);
  }

  getRefreshToken() {
    return localStorage.getItem('refreshtoken');
  }

  removeRefreshToken() {
    localStorage.removeItem('refreshtoken');
  }

  setLocalItem(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  getLocalItem(key: string) {
    return localStorage.getItem(key);
  }

  setLocalObject(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  getLocalObject(key: string) {
    return JSON.parse(localStorage.getItem(key) || '{}');
  }

  removeLocalObject(key: string) {
    localStorage.removeItem(key);
  }

  clearStorage() {
    localStorage.clear();
  }
}
