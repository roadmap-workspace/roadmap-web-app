import { Direction } from '@angular/cdk/bidi';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { Language } from '../../models/language';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  direction: BehaviorSubject<Direction> = new BehaviorSubject<Direction>('ltr');
  language: Language;
  constructor(
    private translateService: TranslateService,
    private storageService: StorageService
  ) {
    this.language = {
      name: localStorage.getItem('lang') || 'en',
      dir: (localStorage.getItem('dir') || 'ltr') as Direction,
    };
  }

  initLanguages() {
    this.translateService.addLangs(['en', 'ar']);
    this.translateService.setDefaultLang(this.language.name);
    this.translateService.use(this.language.name);
    this.storageService.setLocalItem('dir', this.language.dir);
    this.direction.next(this.language.dir);
  }

  selectLanguage(event: CustomEvent) {
    switch (event.detail.value) {
      case 'ar':
        this.selectArabic();
        break;
      case 'en':
        this.selectEnglish();
        break;
      default:
        return;
    }
  }

  selectEnglish() {
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');
    this.direction.next('ltr');
    this.storageService.setLocalItem('lang', 'en');
    this.storageService.setLocalItem('dir', 'ltr');
  }

  selectArabic() {
    this.translateService.setDefaultLang('ar');
    this.translateService.use('ar');
    this.direction.next('rtl');
    this.storageService.setLocalItem('lang', 'ar');
    this.storageService.setLocalItem('dir', 'rtl');
  }
}
