/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
declare let window: any;

export enum GoogleContext {
  signin = 'signin',
  signup = 'signup',
  use = 'use',
}

export interface GoogleOneTabOptions {
  client_id: string;
  auto_select: boolean;
  cancel_on_tap_outside: boolean;
  context: GoogleContext;
}

@Injectable({
  providedIn: 'root',
})
export class GoogleOneTabService {
  options: GoogleOneTabOptions;

  constructor() {
    this.options = {
      client_id:
        '426784544209-uok4l3nf27stigrbbei6dq54c2r62hmn.apps.googleusercontent.com',
      auto_select: false,
      cancel_on_tap_outside: true,
      context: GoogleContext.signin,
    };
  }

  signinWithGoogle(callback: any) {
    const googleScript = document.createElement('script');
    googleScript.setAttribute('src', 'https://accounts.google.com/gsi/client');
    document.head.appendChild(googleScript);
    window.onload = () => {
      window.google.accounts.id.initialize({ ...this.options, callback });
      window.google.accounts.id.prompt((notification: any) => {
        if (notification.isNotDisplayed()) {
        } else if (notification.isSkippedMoment()) {
        } else if (notification.isDismissedMoment()) {
        }
        // continue with another identity provider.
      });
    };
  }
}
