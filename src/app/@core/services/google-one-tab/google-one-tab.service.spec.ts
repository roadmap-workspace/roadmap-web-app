import { TestBed } from '@angular/core/testing';

import { GoogleOneTabService } from './google-one-tab.service';

describe('GoogleOneTabService', () => {
  let service: GoogleOneTabService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoogleOneTabService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
