import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { endpoints } from 'src/app/@api/constants/endpoints.const';
import { environment } from 'src/environments/environment';
import { Profile } from '../models/profile.interface';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  profileEndpoint: string;
  constructor(private http: HttpClient) {
    this.profileEndpoint = `${environment.baseURL}/${endpoints.profile}`;
  }

  getProfile(): Observable<Profile> {
    return this.http.get<Profile>(this.profileEndpoint);
  }

  putProfile(profile: Profile): Observable<Profile> {
    return this.http.put<Profile>(this.profileEndpoint, profile);
  }
}
