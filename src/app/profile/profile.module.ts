import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './components/profile/profile.component';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../@shared/shared.module';
import { MaterialModule } from '../@material/material.module';

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    IonicModule,
    SharedModule,
    MaterialModule,
  ],
})
export class ProfileModule {}
