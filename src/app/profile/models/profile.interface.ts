import { Plan } from 'src/app/@api/models/plan.interface';
import { WorkDomain } from 'src/app/@api/models/work-domain.interface';

export interface Profile {
  _id: string;
  name: string;
  email: string;
  workDomain: WorkDomain;
  plan: Plan;
  website: string;
  about: string;
  workHours: string;
  timeZone: string;
  numOfEmployees: number;
  logo: string;
  coverImage: string;
}
