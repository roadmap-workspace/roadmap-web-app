import { Component, OnInit } from '@angular/core';
import { take, tap } from 'rxjs/operators';
import { fade } from 'src/app/@shared/animations/fade';
import { Statistics } from 'src/app/dashboard/models/statistics.interface';
import { DashboardService } from 'src/app/dashboard/services/dashboard.service';
import { Profile } from '../../models/profile.interface';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [fade],
})
export class ProfileComponent implements OnInit {
  profile: Profile;
  statistics: Statistics;
  isLoaded: boolean;
  isLoadedS: boolean;
  constructor(
    private profileService: ProfileService,
    private dashboardService: DashboardService
  ) {}

  ngOnInit() {
    this.getProfile();
    this.getStatistics();
  }

  getProfile() {
    this.profileService
      .getProfile()
      .pipe(
        tap(() => (this.isLoaded = true)),
        take(1)
      )
      .subscribe((profile) => {
        this.profile = profile;
        console.warn(profile);
      });
  }

  getStatistics() {
    this.dashboardService
      .getStatistics()
      .pipe(
        tap(() => (this.isLoadedS = true)),
        take(1)
      )
      .subscribe((statistics) => {
        this.statistics = statistics;
      });
  }
}
