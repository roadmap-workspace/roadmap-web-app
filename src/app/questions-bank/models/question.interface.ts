import { Option } from './option.interface';

export interface Question {
  _id: string;
  type: 'singleChoice' | 'multipleChoice';
  level: 'easy' | 'medium' | 'hard';
  text: string;
  nodeId?: string;
  options: Option[];
}
