import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { SnackbarService } from 'src/app/@material/services/snackbar.service';
import { fade } from 'src/app/@shared/animations/fade';
import { DeleteDialogComponent } from 'src/app/@shared/components/delete-dialog/delete-dialog.component';
import { Link } from 'src/app/@shared/models/link.interface';
import { Question } from '../../models/question.interface';
import { QuestionsBankService } from '../../services/questions-bank.service';
import { AddQuestionComponent } from '../add-question/add-question.component';
import { UpdateQuestionComponent } from '../update-question/update-question.component';

@Component({
  selector: 'app-node-questions',
  templateUrl: './node-questions.component.html',
  styleUrls: ['./node-questions.component.scss'],
  animations: [fade],
})
export class NodeQuestionsComponent implements OnInit {
  nodeId: string;
  rdId: string;
  deptId: string;
  crumbs: Link[];
  questions: Question[];
  isLoaded: boolean;
  dataSource: MatTableDataSource<Question>;
  columns: string[];
  constructor(
    private activatedRoute: ActivatedRoute,
    private storageService: StorageService,
    private questionsService: QuestionsBankService,
    private snackbarService: SnackbarService,
    private dialog: MatDialog
  ) {
    this.nodeId = this.activatedRoute.snapshot.paramMap.get('id');
    this.rdId = this.activatedRoute.snapshot.paramMap.get('rdId');
    this.deptId = this.storageService.getLocalItem('deptId');
    this.crumbs = this.initCrumbs();
    this.dataSource = new MatTableDataSource<Question>();
    this.columns = ['type', 'level', 'text', 'actions'];
  }

  ngOnInit() {
    this.getQuestions();
  }

  initCrumbs(): Link[] {
    return [
      { label: 'crumb.roadmaps', route: '/panel/roadmaps' },
      {
        label: 'crumb.updateRoadmap',
        route: `/panel/roadmaps/update-roadmap/${this.deptId}/${this.rdId}`,
      },
      {
        label: 'crumb.nodes',
        route: `/panel/roadmaps/questions-bank/${this.rdId}`,
      },
      {
        label: 'crumb.questions',
        route: `/panel/roadmaps/questions-bank/${this.rdId}/nodes/${this.nodeId}`,
      },
    ];
  }

  initTable() {
    this.dataSource = new MatTableDataSource(this.questions);
  }

  getQuestions() {
    this.questionsService
      .getQuestions(this.nodeId)
      .pipe(tap(() => (this.isLoaded = true)))
      .subscribe((questions: Question[]) => {
        this.questions = questions;
        this.initTable();
      });
  }

  openDialog(id: string) {
    let dialogRef = this.dialog.open(DeleteDialogComponent);
    let instance = dialogRef.componentInstance;
    instance.title = 'dialog.caution';
    instance.message = 'dialog.removeQuestion';
    instance.actionMessage = 'btn.deleteQuestion';
    dialogRef.afterClosed().subscribe((result) => {
      if (result.decision) {
        this.questionsService
          .deleteQuestion(this.nodeId, id)
          .subscribe((res) => {
            this.questions = this.questions.filter(
              (question) => id !== question._id
            );
            this.initTable();
          });
      }
    });
  }

  openAddQuestionDialog() {
    let data = {
      nodeId: this.nodeId,
    };
    const dialogRef = this.dialog.open(AddQuestionComponent, {
      data: data,
      width: '75%',
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.decision) {
        this.getQuestions();
      }
    });
  }

  openUpdateDialog(question: Question) {
    const dialogRef = this.dialog.open(UpdateQuestionComponent, {
      data: question,
      width: '75%',
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.decision) {
        this.getQuestions();
        this.snackbarService.openSnackBar(
          'Question is updated successfully!',
          'rule',
          2000
        );
      }
    });
  }
}
