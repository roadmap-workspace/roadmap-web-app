import { Component, Inject, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Question } from '../../models/question.interface';
import { QuestionsBankService } from '../../services/questions-bank.service';

@Component({
  selector: 'app-update-question',
  templateUrl: './update-question.component.html',
  styleUrls: ['./update-question.component.scss'],
})
export class UpdateQuestionComponent implements OnInit {
  question: FormGroup;
  submitted = false;
  isConfirmed = false;
  oneCorrect = true;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Question,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<UpdateQuestionComponent>,
    private questionsService: QuestionsBankService
  ) {
    this.question = this.initQuestionForm();
    this.pushExistingOptionForms();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.question.controls;
  }

  get optionForm() {
    return this.question.controls['options'] as FormArray;
  }

  ngOnInit() {}

  initQuestionForm() {
    return this.fb.group({
      _id: [this.data._id, Validators.required],
      type: [this.data.type, Validators.required],
      level: [this.data.level, Validators.required],
      text: [this.data.text, Validators.required],
      options: this.fb.array([]),
    });
  }

  pushExistingOptionForms() {
    this.data.options.forEach((option: any) => {
      let form = this.fb.group({
        text: [option.text, Validators.required],
        isCorrect: [option.isCorrect ? true : false, Validators.required],
      });
      if (
        this.f['type'].value === 'singleChoice' &&
        this.optionForm.controls.some(
          (control: any) => control.controls['isCorrect'].value == true
        )
      ) {
        this.oneCorrect = true;
        form.controls['isCorrect'].disable();
      } else {
        this.oneCorrect = false;
      }
      this.optionForm.push(form);
    });
  }

  getFormController(i: number, controlName: string): FormControl {
    return <FormControl>(
      (<FormGroup>(<FormArray>this.question.controls['options']).controls[i])
        .controls[controlName]
    );
  }

  addOptionForm() {
    const optionForm = this.fb.group({
      text: ['', Validators.required],
      isCorrect: [false, Validators.required],
    });
    this.optionForm.push(optionForm);
  }

  deleteOptionForm(i: number) {
    if (
      this.f['type'].value === 'singleChoice' &&
      (this.optionForm.controls[i] as any).controls['isCorrect'].value
    ) {
      this.optionForm.controls.forEach((optForm: any) => {
        optForm.controls['isCorrect'].enable();
      });
    }
    this.optionForm.removeAt(i);
    if (
      this.optionForm.controls.some((optForm: any) => {
        optForm.controls['isCorrect'].value === true;
      })
    ) {
      this.oneCorrect = true;
    }
  }

  disableField() {
    let index = {
      i: -1,
      count: 0,
    };
    if (this.f['type'].value === 'singleChoice') {
      this.optionForm.controls.forEach((optForm: any, i) => {
        if (optForm.controls['isCorrect'].value) {
          index.i = i;
          index.count++;
        }
      });
      this.optionForm.controls.forEach((optForm: any, i) => {
        if (i === index.i) {
          return;
        } else if (index.count === 0) {
          optForm.controls['isCorrect'].enable();
        } else {
          optForm.controls['isCorrect'].value
            ? optForm.controls['isCorrect'].enable()
            : optForm.controls['isCorrect'].disable();
        }
      });
    }
    if (
      this.optionForm.controls.some(
        (control: any) => control.controls['isCorrect'].value == true
      )
    ) {
      this.oneCorrect = true;
    } else {
      this.oneCorrect = false;
    }
  }

  handleOptionControls(event) {
    if (
      this.optionForm.controls.some((optForm: any) => {
        optForm.controls['isCorrect'].value === true;
      })
    ) {
      this.oneCorrect = true;
    }
    if (event.value === 'multipleChoice') {
      this.optionForm.controls.forEach((optForm: any) => {
        optForm.controls['isCorrect'].enable();
      });
    } else {
      this.optionForm.controls.forEach((optForm: any) => {
        this.oneCorrect = false;
        optForm.controls['isCorrect'].setValue(false);
      });
    }
  }

  submit(): void {
    this.submitted = true;
    let question: Question = {
      ...this.question.value,
      options: [...this.optionForm.value],
    };
    if (this.question.invalid || !this.oneCorrect) {
      return;
    }
    this.updateQuestion(question);
  }

  updateQuestion(question) {
    this.questionsService.updateQuestion(this.data.nodeId, question).subscribe(
      () => {
        this.isConfirmed = true;
        this.dialogRef.close({ decision: true });
      },
      () => {
        this.dialogRef.close();
      }
    );
  }

  cancel() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    if (!this.isConfirmed) {
      this.dialogRef.close({ decision: false });
    }
  }
}
