import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';
import { filter, pairwise } from 'rxjs/operators';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { Node } from 'src/app/@graph/models/node';
import { fade } from 'src/app/@shared/animations/fade';
import { Link } from 'src/app/@shared/models/link.interface';
import { NodeService } from 'src/app/roadmap/services/node.service';

@Component({
  selector: 'app-view-nodes',
  templateUrl: './view-nodes.component.html',
  styleUrls: ['./view-nodes.component.scss'],
  animations: [fade],
})
export class ViewNodesComponent implements OnInit {
  deptId: string;
  rdId: string;
  crumbs: Link[];
  nodes: Node[] = [];
  isLoaded: boolean;
  constructor(
    private activatedRoute: ActivatedRoute,
    private storageService: StorageService,
    private nodeService: NodeService
  ) {
    this.rdId = this.activatedRoute.snapshot.paramMap.get('id');
    this.deptId = this.storageService.getLocalItem('deptId');
    this.crumbs = this.initCrumbs();
  }

  ngOnInit() {
    this.getNodes();
  }

  initCrumbs(): Link[] {
    return [
      { label: 'crumb.roadmaps', route: '/panel/roadmaps' },
      {
        label: 'crumb.updateRoadmap',
        route: `/panel/roadmaps/update-roadmap/${this.deptId}/${this.rdId}`,
      },
      {
        label: 'crumb.nodes',
        route: `/panel/roadmaps/questions-bank/${this.rdId}`,
      },
    ];
  }

  getNodes() {
    this.nodeService.getNodes(this.rdId).subscribe((nodes: Node[]) => {
      this.nodes = nodes;
      this.isLoaded = true;
    });
  }
}
