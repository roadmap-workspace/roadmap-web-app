import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { fade } from 'src/app/@shared/animations/fade';
import { Link } from 'src/app/@shared/models/link.interface';
import { AddQuestionComponent } from '../add-question/add-question.component';

@Component({
  selector: 'app-empty-questions',
  templateUrl: './empty-questions.component.html',
  styleUrls: ['./empty-questions.component.scss'],
  animations: [fade],
})
export class EmptyQuestionsComponent implements OnInit, AfterViewInit {
  @Input() rdId: string;
  @Input() nodeId: string;
  @Output() add: EventEmitter<any> = new EventEmitter<any>()
  deptId: string;
  crumbs: Link[];
  constructor(private storageService: StorageService) {
    this.deptId = this.storageService.getLocalItem('deptId');
  }

  ngOnInit() {}

  ngAfterViewInit(): void {
    this.crumbs = this.initCrumbs();
  }
  initCrumbs(): Link[] {
    return [
      { label: 'crumb.roadmaps', route: '/panel/roadmaps' },
      {
        label: 'crumb.updateRoadmap',
        route: `/panel/roadmaps/update-roadmap/${this.deptId}/${this.rdId}`,
      },
      {
        label: 'crumb.nodes',
        route: `/panel/roadmaps/questions-bank/${this.rdId}`,
      },
      {
        label: 'crumb.questions',
        route: `/panel/roadmaps/questions-bank/${this.rdId}/nodes/${this.nodeId}`,
      },
    ];
  }
}
