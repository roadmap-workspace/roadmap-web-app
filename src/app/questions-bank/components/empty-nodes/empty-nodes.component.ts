import { Component, Input, OnInit } from '@angular/core';
import { fade } from 'src/app/@shared/animations/fade';

@Component({
  selector: 'app-empty-nodes',
  templateUrl: './empty-nodes.component.html',
  styleUrls: ['./empty-nodes.component.scss'],
  animations: [fade],
})
export class EmptyNodesComponent implements OnInit {
  @Input() rdId: string;
  constructor() {}

  ngOnInit() {}
}
