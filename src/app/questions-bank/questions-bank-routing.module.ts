import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NodeQuestionsComponent } from './components/node-questions/node-questions.component';
import { ViewNodesComponent } from './components/view-nodes/view-nodes.component';

const routes: Routes = [
  { path: ':id', component: ViewNodesComponent },
  { path: ':rdId/nodes/:id', component: NodeQuestionsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuestionsBankRoutingModule {}
