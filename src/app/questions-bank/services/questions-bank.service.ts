import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { endpoints } from 'src/app/@api/constants/endpoints.const';
import { environment } from 'src/environments/environment';
import { Question } from '../models/question.interface';

@Injectable({
  providedIn: 'root',
})
export class QuestionsBankService {
  endpoint: string;
  constructor(private http: HttpClient) {
    this.endpoint = `${environment.baseURL}/${endpoints.nodes}`;
  }

  createQuestion(nodeId: string, question: Question): Observable<Question> {
    return this.http.post<Question>(
      `${this.endpoint}/${nodeId}/${endpoints.questions}`,
      question
    );
  }

  getQuestions(nodeId: string): Observable<Question[]> {
    return this.http.get<Question[]>(
      `${this.endpoint}/${nodeId}/${endpoints.questions}`
    );
  }

  getQuestion(nodeId: string, id: string): Observable<Question> {
    return this.http.get<Question>(
      `${this.endpoint}/${nodeId}/${endpoints.questions}/${id}`
    );
  }

  updateQuestion(nodeId: string, question: Question): Observable<Question> {
    return this.http.patch<Question>(
      `${this.endpoint}/${nodeId}/${endpoints.questions}/${question._id}`,
      question
    );
  }

  deleteQuestion(nodeId: string, id: string): Observable<Question> {
    return this.http.delete<Question>(
      `${this.endpoint}/${nodeId}/${endpoints.questions}/${id}`
    );
  }
}
