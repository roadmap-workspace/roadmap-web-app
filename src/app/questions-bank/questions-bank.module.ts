import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionsBankRoutingModule } from './questions-bank-routing.module';
import { ViewNodesComponent } from './components/view-nodes/view-nodes.component';
import { SharedModule } from '../@shared/shared.module';
import { MaterialModule } from '../@material/material.module';
import { TuiModule } from '../@tui/tui.module';
import { ReactiveFormsModule } from '@angular/forms';
import { EmptyNodesComponent } from './components/empty-nodes/empty-nodes.component';
import { EmptyQuestionsComponent } from './components/empty-questions/empty-questions.component';
import { NodeQuestionsComponent } from './components/node-questions/node-questions.component';
import { AddQuestionComponent } from './components/add-question/add-question.component';
import { UpdateQuestionComponent } from './components/update-question/update-question.component';

@NgModule({
  declarations: [
    ViewNodesComponent,
    EmptyNodesComponent,
    NodeQuestionsComponent,
    EmptyQuestionsComponent,
    AddQuestionComponent,
    UpdateQuestionComponent,
  ],
  imports: [
    CommonModule,
    QuestionsBankRoutingModule,
    SharedModule,
    MaterialModule,
    TuiModule,
    ReactiveFormsModule,
  ],
})
export class QuestionsBankModule {}
