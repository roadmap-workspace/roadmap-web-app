import { Direction } from '@angular/cdk/bidi';
import { Component, OnInit, Optional } from '@angular/core';
import { TitleService } from './@core/services/title/title.service';
import { TranslationService } from './@core/services/translation/translation.service';
import { initializeApp } from 'firebase/app';
import { getAnalytics } from 'firebase/analytics';
import { environment } from 'src/environments/environment';
import { IonRouterOutlet, Platform } from '@ionic/angular';
import { App } from '@capacitor/app';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  direction: Direction;
  constructor(
    private titleService: TitleService,
    private translationService: TranslationService,
    private platform: Platform,
    private location: Location,
    @Optional() private readonly routerOutlet: IonRouterOutlet
  ) {
    this.direction = 'ltr';
    this.translationService.initLanguages();
    this.titleService.changeTitle();
    this.subscribeLayoutDirection();
    this.platform.backButton.subscribeWithPriority(-1, () => {
      if (!this.routerOutlet.canGoBack()) {
        App.exitApp();
      } else {
        this.location.back();
      }
    });
  }

  subscribeLayoutDirection(): void {
    this.translationService.direction.subscribe((dir) => {
      this.direction = dir;
    });
  }

  ngOnInit(): void {
    const app = initializeApp(environment.firebaseConfig);
    const analytics = getAnalytics(app);
  }
}
