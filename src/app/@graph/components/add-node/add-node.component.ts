import { Direction } from '@angular/cdk/bidi';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { Observable, ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { TranslationService } from 'src/app/@core/services/translation/translation.service';
import { accessTypes } from 'src/app/roadmap/constants/access-type.const';
import { AccessType } from 'src/app/roadmap/models/access-type.interface';
import { Roadmap } from 'src/app/roadmap/models/roadmap.interface';
import { NodeService } from 'src/app/roadmap/services/node.service';
import { RoadmapService } from 'src/app/roadmap/services/roadmap.service';
import { Node } from '../../models/node';

@Component({
  selector: 'app-add-node',
  templateUrl: './add-node.component.html',
  styleUrls: ['./add-node.component.scss'],
})
export class AddNodeComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('multiSelect', { static: true }) multiSelect: MatSelect;
  @Output() isAdded: EventEmitter<boolean>;
  accessTypes: AccessType[];
  direction: Direction;
  nodeForm: FormGroup;
  roadmaps$: Observable<Roadmap[]>;
  nodes: Node[];
  filteredNodes: Node[];
  bankMultiFilterCtrl: FormControl = new FormControl();
  public filteredBanksMulti: ReplaySubject<Node[]> = new ReplaySubject<Node[]>(
    1
  );
  submitted = false;
  isConfirmed = false;
  referenceIdReq = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<AddNodeComponent>,
    private translationService: TranslationService,
    private fb: FormBuilder,
    private nodeService: NodeService,
    private roadmapService: RoadmapService
  ) {
    this.isAdded = new EventEmitter<boolean>();
    this.accessTypes = accessTypes;
    this.direction = 'ltr';
    this.subscribeLayoutDirection();
    this.nodeForm = this.initNodeForm();
    this.nodes = this.data.nodes.filter(
      (node: Node) => node.type !== 'roadmap'
    );
    this.filteredNodes = [...this.nodes];
  }

  get f(): { [key: string]: AbstractControl } {
    return this.nodeForm.controls;
  }

  ngOnInit() {
    this.filteredBanksMulti.next(this.nodes.slice());
    this.bankMultiFilterCtrl.valueChanges.subscribe(() => {
      this.filterBanksMulti();
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.setInitialValue();
    }, 100);
  }

  protected filterBanksMulti() {
    if (!this.filteredNodes) {
      return;
    }
    // get the search keyword
    let search = this.bankMultiFilterCtrl.value;
    if (!search) {
      this.filteredBanksMulti.next(this.filteredNodes.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredBanksMulti.next(
      this.filteredNodes.filter(
        (aNode) => aNode.label.toLowerCase().indexOf(search) > -1
      )
    );
  }

  protected setInitialValue() {
    this.filteredBanksMulti.pipe(take(1)).subscribe(() => {
      // setting the compareWith property to a comparison function
      // triggers initializing the selection according to the initial value of
      // the form control (i.e. _initializeSelection())
      // this needs to be done after the filteredBanks are loaded initially
      // and after the mat-option elements are available
      this.multiSelect.compareWith = (a: Node, b: Node) =>
        a && b && a._id === b._id;
    });
  }

  subscribeLayoutDirection(): void {
    this.translationService.direction.subscribe((dir) => {
      this.direction = dir;
    });
  }

  initNodeForm() {
    return this.fb.group({
      roadmapId: [this.data.roadmapId, Validators.required],
      referenceId: [null],
      parents: [[], this.data.nodes.length > 0 ? Validators.required : ''],
      label: [null, Validators.required],
      accessType: [null, Validators.required],
      requiredHoursToStudy: [null, Validators.required],
      quantityOfRequiredQ: [null, Validators.required],
      quantityOfOptionalQ: [null, Validators.required],
      supplements: this.fb.array([]),
    });
  }

  submit(): void {
    this.submitted = true;
    if (this.nodeForm.invalid) {
      return;
    }
    this.addNode();
  }

  addNode() {
    this.nodeService
      .createNode(this.data.roadmapId, this.nodeForm.value)
      .subscribe(
        (nodes: Node[]) => {
          this.isConfirmed = true;
          this.dialogRef.close({ decision: true, nodes: nodes });
        },
        () => {
          this.dialogRef.close();
        }
      );
  }

  getRoadmaps() {
    this.roadmaps$ = this.roadmapService.getCompanyRoadmaps();
  }

  cancel() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    if (!this.isConfirmed) {
      this.dialogRef.close({ decision: false });
    }
  }
}
