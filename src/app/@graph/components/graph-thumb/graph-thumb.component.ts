import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  ConnectorConstraints,
  DiagramComponent,
  NodeConstraints,
  ShapeStyleModel,
} from '@syncfusion/ej2-angular-diagrams';
import {
  NodeModel,
  ConnectorModel,
  DiagramTools,
  Diagram,
  DataBinding,
  ComplexHierarchicalTree,
  SnapConstraints,
  SnapSettingsModel,
  LayoutModel,
  DiagramContextMenu,
} from '@syncfusion/ej2-diagrams';
import { Node } from '../../models/node';

Diagram.Inject(DataBinding, ComplexHierarchicalTree);
Diagram.Inject(DiagramContextMenu);

export interface DataInfo {
  [key: string]: string;
}

@Component({
  selector: 'app-graph-thumb',
  templateUrl: './graph-thumb.component.html',
  styleUrls: ['./graph-thumb.component.scss'],
})
export class GraphThumbComponent implements OnInit, AfterViewInit {
  @Input() nodes: Node[];
  @ViewChild('diagram') diagram: DiagramComponent;
  @ViewChild('diagram') nodeConstraints: NodeConstraints;
  connectorConstraints: ConnectorConstraints;
  dataSource: Object;
  connections: any[];

  constructor() {}

  ngOnInit() {
    this.mapConnections();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.diagram.fitToPage();
    }, 0);
  }
  mapConnections() {
    this.connections = [];
    this.nodes.forEach((node) => {
      node.parents.forEach((parent) => {
        this.connections.push({ id: node._id, parent: parent });
      });
    });
  }

  nodeDefaults(node: NodeModel): NodeModel {
    let style: ShapeStyleModel;
    if ((node as any)?.nodes?.[0]?.annotations?.type) {
      switch ((node as any).nodes[0].annotations.type) {
        case 'roadmap':
          style = {
            fill: '#E5B25D',
            strokeWidth: 0,
          };
          break;
        case 'section':
          style = {
            fill: '#081158',
            strokeWidth: 0,
          };
          break;
        case 'leaf':
          style = {
            fill: '#36AE7C',
            strokeWidth: 0,
          };
          break;
        default:
          break;
      }
    }
    node = {
      width: 90,
      height: 40,
      style: style,
      shape: { type: 'Basic', shape: 'Rectangle', cornerRadius: 10 },
    };
    node.constraints =
      NodeConstraints.Default &&
      ~NodeConstraints.Delete & ~NodeConstraints.Shadow;
    return node;
  }

  created(): void {}
  connDefaults(connector: ConnectorModel): void {
    connector.type = 'Orthogonal';
    connector.cornerRadius = 7;
    connector.targetDecorator.height = 7;
    connector.targetDecorator.width = 7;
    connector.style.strokeColor = '#6d6d6d';
    connector.constraints = ~ConnectorConstraints.Select;
  }

  tool: DiagramTools = DiagramTools.Default;

  snapSettings: SnapSettingsModel = {
    constraints: SnapConstraints.None,
  };

  layout: LayoutModel = {
    type: 'ComplexHierarchicalTree',
    horizontalSpacing: 40,
    verticalSpacing: 40,
    orientation: 'TopToBottom',
    horizontalAlignment: 'Center',
    verticalAlignment: 'Center',
  };
}
