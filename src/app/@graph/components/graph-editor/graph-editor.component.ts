import { BreakpointState } from '@angular/cdk/layout';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  BasicShapeModel,
  ConnectorConstraints,
  Diagram,
  DiagramBeforeMenuOpenEventArgs,
  DiagramComponent,
  DiagramMenuEventArgs,
  IScrollChangeEventArgs,
  NodeConstraints,
  ShadowModel,
  ShapeStyleModel,
  UndoRedo,
} from '@syncfusion/ej2-angular-diagrams';
import {
  NodeModel,
  ConnectorModel,
  DiagramTools,
  SnapConstraints,
  SnapSettingsModel,
  LayoutModel,
  ContextMenuSettingsModel,
} from '@syncfusion/ej2-diagrams';
import { Observable } from 'rxjs';
import { ScreenService } from 'src/app/@core/services/screen/screen.service';
import { SnackbarService } from 'src/app/@material/services/snackbar.service';
import { NodeService } from 'src/app/roadmap/services/node.service';
import { Node } from '../../models/node';
import { AddNodeComponent } from '../add-node/add-node.component';
Diagram.Inject(UndoRedo);
@Component({
  selector: 'app-graph-editor',
  templateUrl: './graph-editor.component.html',
  styleUrls: ['./graph-editor.component.scss'],
})
export class GraphEditorComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() nodes: Node[];
  @Input() tool: string | number;
  @Output() nodeSettings: EventEmitter<Node> = new EventEmitter<Node>();
  @Output() nodesChanged: EventEmitter<Node[]> = new EventEmitter<Node[]>();
  @ViewChild('diagram') diagram: DiagramComponent;
  @ViewChild('diagram') nodeConstraints: NodeConstraints;
  @HostListener('document:keydown', ['$event'])
  selectHandler(event: KeyboardEvent) {
    switch (event.key) {
      case 'n':
        this.useNode();
        break;
      /* case 'c':
        if (!event.ctrlKey) this.useConnector();
        break; */
      case 'd':
        this.useDefault();
        break;
      case 'g':
        this.useGrip();
        break;
      case 'Delete':
        this.useDefault();
        break;
      default:
        break;
    }
  }
  roadmapId: string;
  node: NodeModel;
  connector: ConnectorModel;
  connections: any[];
  drawingshape: BasicShapeModel;
  contextMenuSettings: ContextMenuSettingsModel;
  snapSettings: SnapSettingsModel;
  panStatus: string;
  layout: LayoutModel;
  shadow: ShadowModel;
  isBelowSm$: Observable<BreakpointState>;
  constructor(
    private cdref: ChangeDetectorRef,
    private router: Router,
    private dialog: MatDialog,
    private snackBarService: SnackbarService,
    private nodeService: NodeService,
    private screenService: ScreenService
  ) {
    this.contextMenuSettings = this.initContextMenuSettings();
    this.snapSettings = this.initSnapSettings();
    this.layout = this.initLayout();
    this.roadmapId = this.router.url.split('/editor')[0].split('roadmaps/')[1];
    this.shadow = {
      angle: 90,
      distance: 2,
      opacity: 0.2,
      color: 'black',
    };
    this.isBelowSm$ = this.screenService.isBelowSm();
  }

  initContextMenuSettings(): ContextMenuSettingsModel {
    return {
      show: true,
      showCustomMenuOnly: false,
      items: [
        {
          id: 'settings',
          text: 'Settings',
          iconCss: 'e-actions',
        },
      ],
    };
  }

  initSnapSettings(): SnapSettingsModel {
    return {
      constraints: SnapConstraints.SnapToObject | SnapConstraints.ShowLines,
      snapObjectDistance: 10,
      snapAngle: 10,
      snapLineColor: 'red',
    };
  }

  initLayout(): LayoutModel {
    return {
      type: 'ComplexHierarchicalTree',
      horizontalSpacing: 30,
      verticalSpacing: 30,
      orientation: 'LeftToRight',
      horizontalAlignment: 'Center',
      verticalAlignment: 'Center',
    };
  }

  ngOnInit() {
    this.mapConnections();
  }

  ngAfterViewInit(): void {
    this.nodeConstraints = NodeConstraints.Default | NodeConstraints.Shadow;
    setTimeout(() => {
      document.querySelectorAll('rect[id$=_shadow]').forEach((element: any) => {
        element.setAttribute('rx', '10');
        this.diagram.fitToPage();
      });
    }, 0);

    this.cdref.detectChanges();
  }

  ngOnChanges(changes: any): void {
    switch (changes.tool.currentValue) {
      case 1:
        setTimeout(() => {
          this.useNode();
        }, 100);
        break;
      /* case 2:
        this.useConnector();
        break; */
      case 3:
        this.useGrip();
        break;
      case 4:
        this.useDefault();
        break;
      default:
        break;
    }
  }

  created(event: any) {
    this.diagram.dataBind();
  }

  nodeDefaults(node: NodeModel): NodeModel {
    let style: ShapeStyleModel;
    if ((node as any)?.nodes?.[0]?.annotations?.type) {
      switch ((node as any).nodes[0].annotations.type) {
        case 'roadmap':
          style = {
            fill: '#E5B25D',
            strokeWidth: 0,
          };
          break;
        case 'section':
          style = {
            fill: '#081158',
            strokeWidth: 0,
          };
          break;
        case 'leaf':
          style = {
            fill: '#36AE7C',
            strokeWidth: 0,
          };
          break;
        default:
          break;
      }
    }
    node = {
      minWidth: 90,
      minHeight: 40,
      style: style,
      shape: { type: 'Basic', shape: 'Rectangle', cornerRadius: 10 },
    };
    return node;
  }

  connDefaults(connector: ConnectorModel): void {
    connector.type = 'Orthogonal';
    connector.cornerRadius = 7;
    connector.targetDecorator.height = 7;
    connector.targetDecorator.width = 7;
    connector.style.strokeColor = '#6d6d6d';
  }

  mapConnections() {
    this.connections = [];
    this.nodes.forEach((node) => {
      node.parents.forEach((parent) => {
        this.connections.push({ id: node._id, parent: parent });
      });
    });
  }

  useNode() {
    this.drawingshape = { type: 'Basic', shape: 'Rectangle' };
    this.node = {
      shape: this.drawingshape,
    };
    this.diagram.drawingObject = this.node;
    this.diagram.tool = DiagramTools.ContinuousDraw;
    this.diagram.dataBind();
  }

  useConnector() {
    this.connector = {
      type: 'Straight',
      segments: [{ type: 'Orthogonal' }],
    };
    this.diagram.drawingObject = this.connector;
    this.diagram.tool = DiagramTools.ContinuousDraw;
    this.diagram.dataBind();
  }

  useGrip() {
    this.diagram.tool = DiagramTools.ZoomPan;
  }

  useDefault() {
    this.diagram.tool = DiagramTools.Default;
  }

  detectCollectionChange(event: any) {
    if (this.isAddNode(event)) {
      this.openRoadmapDialog();
    } else if (this.isRemoveNode(event)) {
      let node: Node = event.element.nodes?.[0].annotations;
      if (!node) {
        return;
      }
      this.deleteNode(node);
    } else if (this.isAddConnector(event)) {
      if (this.isValidNodeAddtion(event)) {
        if (this.isAlreadyThere(event)) {
          this.snackBarService.openSnackBar(
            'Such connector already exist',
            'warning',
            3000
          );
          setTimeout(() => {
            this.diagram.undo();
          }, 0);
          return;
        } else {
          let targetID = event.element.properties.targetID;
          let sourceID = event.element.properties.sourceID;
          let oldNode = this.nodes.find((node) => node._id === targetID);
          oldNode.parents.push(sourceID);
          this.updateNode(oldNode);
        }
      } else {
        this.snackBarService.openSnackBar(
          'Connector should have both source and target',
          'do_not_disturb_on',
          3000
        );
        setTimeout(() => {
          this.diagram.undo();
        }, 0);
      }
    }
  }

  isAddNode(event: any) {
    return (
      event.state === 'Changed' &&
      event.type === 'Addition' &&
      event.element.propName === 'nodes'
    );
  }

  isRemoveNode(event: any) {
    return (
      event.state === 'Changed' &&
      event.type === 'Removal' &&
      event.element.propName === 'nodes'
    );
  }

  isAddConnector(event: any) {
    return (
      event.state === 'Changed' &&
      event.type === 'Addition' &&
      event.element.propName === 'connectors'
    );
  }

  isValidNodeAddtion(event: any) {
    let fromNode = event.element.properties.sourceID;
    let toNode = event.element.properties.targetID;
    return fromNode && toNode;
  }

  isAlreadyThere(event: any) {
    let existCounter = 0;
    let fromNode = event.element.properties.sourceID;
    let toNode = event.element.properties.targetID;
    this.diagram.connectors.forEach((connector: any) => {
      if (
        connector.properties.sourceID === fromNode &&
        connector.properties.targetID === toNode
      ) {
        existCounter++;
      }
    });
    if (existCounter > 1) return true;
    return false;
  }
  detectSelectionChange(event: any) {
    if (event.type !== 'Addition') {
      let potentialNode = this.diagram.selectedItems?.nodes?.[0] as any;
      if (potentialNode) {
        (this.diagram.selectedItems?.nodes?.[0] as any).nodes?.forEach(
          (node) => {
            if (
              node.annotations.type !== 'leaf' &&
              node.annotations.type !== 'roadmap'
            ) {
              this.diagram.nodes.forEach((node) => {
                node.constraints =
                  NodeConstraints.Default & ~NodeConstraints.Delete;
              });
              this.diagram.connectors.forEach((connector) => {
                connector.constraints =
                  ConnectorConstraints.Default & ~ConnectorConstraints.Delete;
              });
            } else {
              this.diagram.nodes.forEach((node) => {
                node.constraints = NodeConstraints.Default;
              });
              this.diagram.connectors.forEach((connector) => {
                connector.constraints = ConnectorConstraints.Default;
              });
            }
          }
        );
      } else {
        this.diagram.connectors.forEach((connector) => {
          connector.constraints =
            ConnectorConstraints.Default & ~ConnectorConstraints.Delete;
        });
      }
    }
  }

  detectConnectionChange(event: any) {
    if (event.state === 'Changed') {
      let oldValue = event.oldValue.nodeId;
      let newValue = event.newValue.nodeId;
      let fromNode = event.connector.properties.sourceID;
      let toNode = event.connector.properties.targetID;
      if (!fromNode && !toNode) {
        this.snackBarService.openSnackBar(
          'Connector should have both source and target',
          'do_not_disturb_on',
          3000
        );
        setTimeout(() => {
          this.diagram.undo();
        }, 1000);
        return;
      }
      // One of the ends is loose
      if (!fromNode || !toNode) {
        this.snackBarService.openSnackBar(
          'Connector should have both source and target',
          'do_not_disturb_on',
          3000
        );
        setTimeout(() => {
          this.diagram.undo();
        }, 0);
        return;
      }
      // Loop case
      if (fromNode === toNode) {
        this.snackBarService.openSnackBar(
          'Loop nodes are not allowed',
          'do_not_disturb_on',
          3000
        );
        setTimeout(() => {
          this.diagram.undo();
        }, 0);
        return;
      }
      if (event.connectorEnd === 'ConnectorSourceEnd') {
        let oldNode = this.nodes.find((node) => node._id == toNode);
        oldNode.parents = oldNode.parents.filter(
          (parent) => parent != oldValue
        );
        oldNode.parents.push(newValue);
        this.updateNode(oldNode);
      } else {
        let oldNode = this.nodes.find((node) => node._id == oldValue);
        oldNode.parents = oldNode.parents.filter(
          (parent) => parent != fromNode
        );
        this.updateNode(oldNode);
      }
    }
  }

  updateNode(node: Node) {
    this.nodeService.updateNode(node.roadmapId, node).subscribe(
      (nodes: Node[]) => {
        this.nodesChanged.emit(nodes);
      },
      (err) => {
        console.error(err);
        this.diagram.undo();
        this.snackBarService.openSnackBar(err.error.message, 'dangerous', 4000);
      }
    );
  }

  deleteNode(node: Node) {
    this.nodeService.deleteNode(node.roadmapId, node._id).subscribe(
      (res: Node[]) => {
        this.nodesChanged.emit(res);
      },
      (err) => {
        this.nodesChanged.emit(this.nodes);
        this.snackBarService.openSnackBar(err.error.message, 'dangerous', 4000);
      }
    );
  }

  scrollChange(args: IScrollChangeEventArgs) {
    this.panStatus = args.panState;
  }

  contextMenuClick(args: DiagramMenuEventArgs) {
    if (args.item.id === 'settings') {
      this.nodeSettings.emit(
        (this.diagram.selectedItems?.nodes[0] as any).nodes[0]?.annotations
      );
    }
  }

  contextMenuOpen(args: DiagramBeforeMenuOpenEventArgs) {
    args.hiddenItems.push('diagram_contextMenu_grouping');
    (this.diagram.selectedItems as any).properties.nodes.length > 1 &&
      args.hiddenItems.push('settings');
  }

  openRoadmapDialog() {
    let data = {
      nodes: this.nodes,
      roadmapId: this.roadmapId,
    };
    const dialogRef = this.dialog.open(AddNodeComponent, {
      data: data,
      width: '100%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.decision) {
        this.nodesChanged.emit(result.nodes);
      } else {
        this.diagram.undo();
      }
    });
  }
}
