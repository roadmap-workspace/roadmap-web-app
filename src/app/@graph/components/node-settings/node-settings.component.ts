import {
  Component,
  ComponentRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { SnackbarService } from 'src/app/@material/services/snackbar.service';
import { accessTypes } from 'src/app/roadmap/constants/access-type.const';
import { AccessType } from 'src/app/roadmap/models/access-type.interface';
import { CompanyRoadmap } from 'src/app/roadmap/models/company-roadmap.interface';
import { NodeService } from 'src/app/roadmap/services/node.service';
import { RoadmapService } from 'src/app/roadmap/services/roadmap.service';
import { Node } from '../../models/node';

@Component({
  selector: 'app-node-settings',
  templateUrl: './node-settings.component.html',
  styleUrls: ['./node-settings.component.scss'],
})
export class NodeSettingsComponent implements OnChanges, OnDestroy {
  @Input() nodeSettings: Node;
  @Output() nodeUpdate: EventEmitter<Node[]>;
  @Output() toggle: EventEmitter<any>;
  accessTypes: AccessType[];
  roadmaps$: Observable<CompanyRoadmap[]>;
  nodeForm: FormGroup;
  isOpen: boolean;
  submitted: boolean;
  referenceIdReq: boolean;
  constructor(
    private fb: FormBuilder,
    private snackBarService: SnackbarService,
    private nodeService: NodeService,
    private roadmapService: RoadmapService
  ) {
    this.nodeUpdate = new EventEmitter<Node[]>();
    this.toggle = new EventEmitter<any>();
    this.accessTypes = accessTypes;
    this.isOpen = false;
    this.submitted = false;
    this.referenceIdReq = false;
  }

  get f(): { [key: string]: AbstractControl } {
    return this.nodeForm.controls;
  }

  get supplementForm() {
    return this.nodeForm.controls['supplements'] as FormArray;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.nodeSettings.currentValue) {
      this.isOpen = true;
      this.nodeForm = this.initForm(changes.nodeSettings.currentValue);
      this.pushExistingSupplementForms(
        changes.nodeSettings.currentValue.supplements
      );
    }
  }

  initForm(node: any) {
    if (node.referenceId) {
      this.referenceIdReq = true;
      this.getRoadmaps();
    }
    return this.fb.group({
      _id: [node._id, Validators.required],
      roadmapId: [node.roadmapId, Validators.required],
      referenceId: [node.referenceId],
      parents: [node.parents],
      label: [node.label, Validators.required],
      accessType: [node.accessType, Validators.required],
      requiredHoursToStudy: [node.requiredHoursToStudy, Validators.required],
      quantityOfRequiredQ: [node.quantityOfRequiredQ, Validators.required],
      quantityOfOptionalQ: [node.quantityOfOptionalQ, Validators.required],
      supplements: this.fb.array([]),
      type: [this.nodeSettings.type, Validators.required],
    });
  }

  pushExistingSupplementForms(supplements: any) {
    supplements.forEach((supplement: any) => {
      let form = this.fb.group({
        supplement: [supplement],
      });
      this.supplementForm.push(form);
    });
  }

  getFormController(i: number, controlName: string): FormControl {
    return <FormControl>(
      (<FormGroup>(
        (<FormArray>this.supplementForm.controls['supplement']).controls[i]
      )).controls[controlName]
    );
  }

  addSupplementForm() {
    const supplemetForm = this.fb.group({
      supplement: ['', Validators.required],
    });
    this.supplementForm.push(supplemetForm);
  }

  deleteSupplementForm(i: number) {
    this.supplementForm.removeAt(i);
  }

  getRoadmaps() {
    this.roadmaps$ = this.roadmapService.getCompanyRoadmaps();
  }

  submit() {
    if (!this.nodeForm.valid) {
      return;
    }
    if (!this.referenceIdReq) {
      this.f['referenceId'].setValue(null);
    }
    let updatedNode: Node = {
      ...this.nodeForm.value,
      supplements: [
        ...this.supplementForm.value.map(({ supplement }) => supplement),
      ],
    };

    this.nodeService.updateNode(updatedNode.roadmapId, updatedNode).subscribe(
      (res: any) => {
        this.nodeUpdate.emit(res);
        this.snackBarService.openSnackBar(
          'Node is updated successfully',
          'sync',
          2000
        );
        this.isOpen = false;
        this.emitToggle();
      },
      (err) => {
        console.error(err);
        this.snackBarService.openSnackBar(err.error.message, 'dangerous', 4000);
      }
    );
  }

  emitToggle() {
    this.toggle.emit();
  }

  ngOnDestroy(): void {
    this.emitToggle();
  }
}
