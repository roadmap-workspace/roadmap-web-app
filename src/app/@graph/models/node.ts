export interface Node {
  _id: string;
  roadmapId?: string;
  label: string;
  referenceId: string;
  requiredHoursToStudy: number;
  type: string;
  accessType: string;
  quantityOfRequiredQ: number;
  quantityOfOptionalQ: number;
  parents: string[];
  supplements: string[];
}
