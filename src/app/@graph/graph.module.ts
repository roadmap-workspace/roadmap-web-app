import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DiagramModule,
  HierarchicalTreeService,
  DataBindingService,
  DiagramContextMenuService,
  SnappingService,
} from '@syncfusion/ej2-angular-diagrams';
import { GraphThumbComponent } from './components/graph-thumb/graph-thumb.component';
import { TranslateModule } from '@ngx-translate/core';
import { GraphEditorComponent } from './components/graph-editor/graph-editor.component';
import { MaterialModule } from '../@material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NodeSettingsComponent } from './components/node-settings/node-settings.component';
import { TuiModule } from '../@tui/tui.module';
import { AddNodeComponent } from './components/add-node/add-node.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

const utils = [
  GraphThumbComponent,
  GraphEditorComponent,
  NodeSettingsComponent,
  AddNodeComponent,
];
@NgModule({
  declarations: [...utils],
  imports: [
    CommonModule,
    DiagramModule,
    MaterialModule,
    TuiModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,
    NgxMatSelectSearchModule
  ],
  exports: [...utils],
  providers: [
    HierarchicalTreeService,
    DataBindingService,
    DiagramContextMenuService,
    SnappingService,
  ],
})
export class GraphModule {}
