import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { endpoints } from 'src/app/@api/constants/endpoints.const';
import { environment } from 'src/environments/environment';
import { Statistics } from '../models/statistics.interface';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  statistics: string;
  constructor(private http: HttpClient) {
    this.statistics = `${environment.baseURL}/${endpoints.statistics}`;
  }

  getStatistics(): Observable<Statistics> {
    return this.http.get<Statistics>(`${this.statistics}`);
  }
}
