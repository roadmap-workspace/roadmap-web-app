import {
  ApexAxisChartSeries,
  ApexChart,
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexStroke,
  ApexXAxis,
} from 'ng-apexcharts';

export type BarChartOptions = {
  id: string;
  series: ApexAxisChartSeries;
  barChart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
};
