import {
  ApexChart,
  ApexNonAxisChartSeries,
  ApexResponsive,
} from 'ng-apexcharts';

export type PieChartOptions = {
  series: ApexNonAxisChartSeries;
  pieChart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};
