import { Component, OnInit, ViewChild } from '@angular/core';
import { ApexChart, ChartComponent } from 'ng-apexcharts';
import { take, tap } from 'rxjs/operators';
import { BarChartOptions } from '../../types/bar-chart-options.type';
import { Statistics } from '../../models/statistics.interface';
import { DashboardService } from '../../services/dashboard.service';
import { PieChartOptions } from '../../types/pie-chart-options.type';
import { fade } from 'src/app/@shared/animations/fade';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [fade],
})
export class DashboardComponent implements OnInit {
  @ViewChild('barChart') barChart: ApexChart;
  @ViewChild('pieChart') pieChart: ApexChart;
  barChartOptions: Partial<BarChartOptions>;
  pieChartOptions: Partial<PieChartOptions>;
  isLoaded: boolean;
  statistics: Statistics;
  constructor(private dashboardService: DashboardService) {
    this.isLoaded = false;
  }

  ngOnInit(): void {
    this.getStatistics();
  }

  initBarChartData(): void {
    this.barChartOptions = {
      id: 'registeredAccounts',
      series: this.statistics.lineChart.series,
      barChart: {
        type: 'bar',
        height: 350,
      },
      xaxis: {
        categories: this.statistics.lineChart.categories,
      },
    };
  }

  initPieChartData(): void {
    this.pieChartOptions = {
      series: this.statistics.followersPerDeptPieChart.series,
      pieChart: {
        height: 350,
        type: 'pie',
      },
      labels: this.statistics.followersPerDeptPieChart.labels,
    };
  }

  getStatistics() {
    this.dashboardService
      .getStatistics()
      .pipe(
        tap(() => (this.isLoaded = true)),
        take(1)
      )
      .subscribe((statistics) => {
        console.warn(statistics);
        this.statistics = statistics;
        this.initBarChartData();
        this.initPieChartData();
      });
  }
}
