import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-stat-card',
  templateUrl: './stat-card.component.html',
  styleUrls: ['./stat-card.component.scss'],
})
export class StatCardComponent implements OnInit {
  @Input() title: string;
  @Input() stat: number;
  @Input() color: string;
  @Input() icon: string;
  @Input() percentage: string;
  @Input() phrase: string;
  @Input() isUp: boolean;
  @Input() bg: string;

  constuctor() {}

  ngOnInit() {}
}
