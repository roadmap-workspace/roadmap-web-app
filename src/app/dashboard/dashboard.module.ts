import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { IonicModule } from '@ionic/angular';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MaterialModule } from '../@material/material.module';
import { SharedModule } from '../@shared/shared.module';
import { StatCardComponent } from './components/stat-card/stat-card.component';

@NgModule({
  declarations: [DashboardComponent, StatCardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    IonicModule,
    NgApexchartsModule,
    MaterialModule,
    SharedModule,
  ],
})
export class DashboardModule {}
