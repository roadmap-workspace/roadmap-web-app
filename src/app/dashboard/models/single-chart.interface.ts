export interface SingleChart {
  labels: string[];
  ids: string[];
  series: number[];
}
