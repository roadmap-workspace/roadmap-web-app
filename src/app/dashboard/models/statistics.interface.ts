import { ComplexChart } from './complex-chart.interface';
import { SingleChart } from './single-chart.interface';

export interface Statistics {
  departmentsCount: number;
  roadmapsCount: number;
  followersCount: number;
  activeLearnersCount: number;
  followersPerDeptPieChart: SingleChart;
  lineChart: ComplexChart;
}
