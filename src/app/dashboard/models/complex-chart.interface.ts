import { ChartSeriesEntry } from './chart-series-entry.interface';

export interface ComplexChart {
  series: ChartSeriesEntry[];
  categories: string[];
}
