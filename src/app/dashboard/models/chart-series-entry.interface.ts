export interface ChartSeriesEntry {
  name: string;
  data: number[];
}
