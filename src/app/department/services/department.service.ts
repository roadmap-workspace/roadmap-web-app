import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { endpoints } from 'src/app/@api/constants/endpoints.const';
import { environment } from 'src/environments/environment';
import { Department } from '../models/department.interface';

@Injectable({
  providedIn: 'root',
})
export class DepartmentService {
  endpoint: string;
  constructor(private http: HttpClient) {
    this.endpoint = `${environment.baseURL}/${endpoints.departments}`;
  }

  createDepartment(department: Department): Observable<Department> {
    return this.http.post<Department>(this.endpoint, department);
  }

  getDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(this.endpoint);
  }

  getDepartment(id: string): Observable<Department> {
    return this.http.get<Department>(`${this.endpoint}/${id}`);
  }

  updateDepartment(id: string, department: Department): Observable<Department> {
    return this.http.patch<Department>(`${this.endpoint}/${id}`, department);
  }

  deleteDepartment(id: string): Observable<Department> {
    return this.http.delete<Department>(`${this.endpoint}/${id}`);
  }
}
