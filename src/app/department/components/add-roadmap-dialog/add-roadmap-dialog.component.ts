import { Direction } from '@angular/cdk/bidi';
import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslationService } from 'src/app/@core/services/translation/translation.service';
import { RoadmapService } from 'src/app/roadmap/services/roadmap.service';

@Component({
  selector: 'app-add-roadmap-dialog',
  templateUrl: './add-roadmap-dialog.component.html',
  styleUrls: ['./add-roadmap-dialog.component.scss'],
})
export class AddRoadmapDialogComponent implements OnInit, OnDestroy {
  @Output() isAdded: EventEmitter<boolean>;
  direction: Direction;
  roadmap: FormGroup;
  submitted = false;
  isConfirmed = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: string,
    private translationService: TranslationService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AddRoadmapDialogComponent>,
    private roadmapService: RoadmapService
  ) {
    this.isAdded = new EventEmitter<boolean>();
    this.direction = 'ltr';
    this.subscribeLayoutDirection();
    this.roadmap = this.initRoadmapForm();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.roadmap.controls;
  }

  ngOnInit() {}

  subscribeLayoutDirection(): void {
    this.translationService.direction.subscribe((dir) => {
      this.direction = dir;
    });
  }

  initRoadmapForm() {
    return this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  submit(): void {
    this.submitted = true;
    if (this.roadmap.invalid) {
      return;
    }
    this.addRoadmap();
  }

  addRoadmap() {
    this.roadmapService.createRoadmap(this.data, this.roadmap.value).subscribe(
      () => {
        this.isConfirmed = true;
        this.dialogRef.close({ decision: true });
      },
      () => {
        this.dialogRef.close();
      }
    );
  }

  cancel() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    if (!this.isConfirmed) {
      this.dialogRef.close({ decision: false });
    }
  }
}
