import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { fade } from 'src/app/@shared/animations/fade';
import { Roadmap } from 'src/app/roadmap/models/roadmap.interface';
import { RoadmapService } from 'src/app/roadmap/services/roadmap.service';
import { AddRoadmapDialogComponent } from '../add-roadmap-dialog/add-roadmap-dialog.component';

@Component({
  selector: 'app-department-roadmap',
  templateUrl: './department-roadmap.component.html',
  styleUrls: ['./department-roadmap.component.scss'],
  animations: [fade],
})
export class DepartmentRoadmapComponent implements OnInit {
  isLoaded = false;
  roadmaps: Roadmap[];
  constructor(
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private roadmapService: RoadmapService
  ) {
    this.roadmaps = [];
  }

  ngOnInit() {
    this.getRoadmaps();
  }

  getRoadmaps() {
    this.roadmapService.getRoadmaps(this.getID()).subscribe((roadmaps) => {
      this.roadmaps = roadmaps;
      this.isLoaded = true;
    });
  }

  getID() {
    return this.activatedRoute.snapshot.paramMap.get('id');
  }

  openRoadmapDialog() {
    const dialogRef = this.dialog.open(AddRoadmapDialogComponent, {
      data: this.getID(),
      width: '75%',
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.decision) {
        this.getRoadmaps();
      }
    });
  }
}
