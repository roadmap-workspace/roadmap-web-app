import { Component, OnInit } from '@angular/core';
import { fade } from 'src/app/@shared/animations/fade';
import { Department } from '../../models/department.interface';
import { DepartmentService } from '../../services/department.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss'],
  animations: [fade],
})
export class DepartmentComponent implements OnInit {
  isLoaded: boolean;
  departments: Department[];
  constructor(private departmentService: DepartmentService) {
    this.departments = [];
  }

  ngOnInit() {
    this.getDepartments();
  }

  getDepartments() {
    this.departmentService.getDepartments().subscribe((departments) => {
      this.departments = departments;
      this.isLoaded = true;
    });
  }
}
