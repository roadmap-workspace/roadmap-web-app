import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { DepartmentService } from '../../services/department.service';
import { Link } from 'src/app/@shared/models/link.interface';
import { fade } from 'src/app/@shared/animations/fade';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.scss'],
  animations: [fade],
})
export class AddDepartmentComponent implements OnInit {
  department: FormGroup;
  crumbs: Link[];
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private departmentService: DepartmentService,
    private router: Router
  ) {
    this.crumbs = this.initCrumbs();
    this.department = this.initDepartment();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.department.controls;
  }

  ngOnInit() {}

  initCrumbs(): Link[] {
    return [
      { label: 'crumb.departments', route: '/panel/departments' },
      {
        label: 'crumb.addDepartment',
        route: '/panel/departments/add-department',
      },
    ];
  }

  initDepartment() {
    return this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  submit(): void {
    this.submitted = true;
    if (this.department.invalid) {
      return;
    }
    this.addDepartment();
  }

  addDepartment() {
    this.departmentService.createDepartment(this.department.value).subscribe(
      (res) => {
        this.router.navigate(['/panel/departments']);
      },
      (err) => {
        console.error(err);
      }
    );
  }
}
