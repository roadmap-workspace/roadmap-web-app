import { Component, OnInit } from '@angular/core';
import { fade } from 'src/app/@shared/animations/fade';

@Component({
  selector: 'app-empty-department',
  templateUrl: './empty-department.component.html',
  styleUrls: ['./empty-department.component.scss'],
  animations: [fade],
})
export class EmptyDepartmentComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
