import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { fade } from 'src/app/@shared/animations/fade';
import { DeleteDialogComponent } from 'src/app/@shared/components/delete-dialog/delete-dialog.component';
import { Link } from 'src/app/@shared/models/link.interface';
import { Department } from '../../models/department.interface';
import { DepartmentService } from '../../services/department.service';

@Component({
  selector: 'app-update-department',
  templateUrl: './update-department.component.html',
  styleUrls: ['./update-department.component.scss'],
  animations: [fade],
})
export class UpdateDepartmentComponent implements OnInit {
  department: FormGroup;
  crumbs: Link[];
  submitted = false;
  isLoaded = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private departmentService: DepartmentService
  ) {
    this.crumbs = this.initCrumbs();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.department.controls;
  }

  ngOnInit() {
    this.getID();
  }

  initCrumbs(): Link[] {
    return [
      { label: 'crumb.departments', route: '/panel/departments' },
      {
        label: 'crumb.updateDepartment',
        route: '/panel/departments/update-department',
      },
    ];
  }
  getID() {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    if (id) {
      this.getDepartment(id);
    }
  }

  getDepartment(id: string) {
    this.departmentService.getDepartment(id).subscribe((department) => {
      this.department = this.initDepartment(department);
      this.isLoaded = true;
    });
  }

  initDepartment(department: Department) {
    return this.fb.group({
      _id: [department._id, Validators.required],
      name: [department.name, Validators.required],
      description: [department.description, Validators.required],
    });
  }

  submit(): void {
    this.submitted = true;
    if (this.department.invalid) {
      return;
    }
    this.updateDepartment();
  }

  updateDepartment() {
    this.departmentService
      .updateDepartment(this.department.value._id, this.department.value)
      .subscribe(
        () => {
          this.router.navigate(['/panel/departments']);
        },
        (err) => {
          console.error(err);
        }
      );
  }

  openDialog() {
    let dialogRef = this.dialog.open(DeleteDialogComponent);
    let instance = dialogRef.componentInstance;
    instance.title = 'dialog.caution';
    instance.message = 'dialog.removeDepartment';
    instance.actionMessage = 'btn.deleteDepartment';
    dialogRef.afterClosed().subscribe((result) => {
      if (result.decision) {
        this.departmentService
          .deleteDepartment(this.department.value._id)
          .subscribe(() => {
            this.router.navigate(['/panel/departments']);
          });
      }
    });
  }
}
