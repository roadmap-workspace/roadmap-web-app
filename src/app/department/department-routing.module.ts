import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddDepartmentComponent } from './components/add-department/add-department.component';
import { DepartmentComponent } from './components/department/department.component';
import { UpdateDepartmentComponent } from './components/update-department/update-department.component';

const routes: Routes = [
  { path: '', component: DepartmentComponent },
  {
    path: 'add-department',
    component: AddDepartmentComponent,
    data: {
      title: 'Add Department',
    },
  },
  { path: ':id', component: UpdateDepartmentComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DepartmentRoutingModule {}
