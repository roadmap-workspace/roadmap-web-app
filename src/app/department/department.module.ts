import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartmentRoutingModule } from './department-routing.module';
import { IonicModule } from '@ionic/angular';
import { MaterialModule } from '../@material/material.module';
import { TuiModule } from '../@tui/tui.module';
import { SharedModule } from '../@shared/shared.module';
import { EmptyDepartmentComponent } from './components/empty-department/empty-department.component';
import { AddDepartmentComponent } from './components/add-department/add-department.component';
import { DepartmentComponent } from './components/department/department.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UpdateDepartmentComponent } from './components/update-department/update-department.component';
import { DepartmentRoadmapComponent } from './components/department-roadmap/department-roadmap.component';
import { AddRoadmapDialogComponent } from './components/add-roadmap-dialog/add-roadmap-dialog.component';

@NgModule({
  declarations: [
    EmptyDepartmentComponent,
    DepartmentComponent,
    AddDepartmentComponent,
    UpdateDepartmentComponent,
    DepartmentRoadmapComponent,
    AddRoadmapDialogComponent,
  ],
  imports: [
    CommonModule,
    DepartmentRoutingModule,
    IonicModule,
    MaterialModule,
    TuiModule,
    SharedModule,
    ReactiveFormsModule,
  ],
})
export class DepartmentModule {}
