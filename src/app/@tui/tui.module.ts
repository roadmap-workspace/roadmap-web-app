import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TuiButtonModule } from '@taiga-ui/core';
import { TuiSvgModule } from '@taiga-ui/core';
import { TuiHostedDropdownModule } from '@taiga-ui/core';
import { TuiDataListModule } from '@taiga-ui/core';
import { TuiActiveZoneModule } from '@taiga-ui/cdk';
import { TuiBadgeModule } from '@taiga-ui/kit';
import { TuiAccordionModule } from '@taiga-ui/kit';
import { TuiSidebarModule } from '@taiga-ui/addon-mobile';
const modules = [
  TuiBadgeModule,
  TuiButtonModule,
  TuiSvgModule,
  TuiAccordionModule,
  TuiHostedDropdownModule,
  TuiDataListModule,
  TuiSidebarModule,
  TuiActiveZoneModule,
];

@NgModule({
  imports: [CommonModule],
  exports: [...modules],
})
export class TuiModule {}
