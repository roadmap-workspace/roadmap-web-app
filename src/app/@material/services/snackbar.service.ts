import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from 'src/app/@shared/components/snack-bar/snack-bar.component';

@Injectable({
  providedIn: 'root',
})
export class SnackbarService {
  constructor(private snackBar: MatSnackBar) {}

  openSnackBar(message: string, icon: string, duration: number) {
    this.snackBar.openFromComponent(SnackBarComponent, {
      duration: duration,
      data: { message, icon },
    });
  }
}
