import { Component, OnInit } from '@angular/core';
import { fade } from 'src/app/@shared/animations/fade';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  animations: [fade]
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
