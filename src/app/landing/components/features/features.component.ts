import { Component, OnInit } from '@angular/core';
import { fade } from 'src/app/@shared/animations/fade';
import { features } from '../../constants/features.const';
import { Feature } from '../../models/feature.interface';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss'],
  animations: [fade],
})
export class FeaturesComponent implements OnInit {
  features: Feature[];
  constructor() {
    this.features = features;
  }

  ngOnInit() {}
}
