import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { LookUpService } from 'src/app/@api/services/look-up/look-up.service';
import { SnackbarService } from 'src/app/@material/services/snackbar.service';
import { fade } from 'src/app/@shared/animations/fade';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
  animations: [fade],
})
export class ContactUsComponent implements OnInit {
  reporter: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private lookupService: LookUpService,
    private snackbarService: SnackbarService
  ) {
    this.reporter = this.fb.group({
      fullName: ['', Validators.required],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6}$'),
        ],
      ],
      message: ['', Validators.required],
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.reporter.controls;
  }

  ngOnInit(): void {}

  submit(formDirective): void {
    this.submitted = true;
    if (this.reporter.invalid) {
      return;
    }
    this.sendFeedback(formDirective);
  }

  sendFeedback(formDirective) {
    this.lookupService.postFeedback(this.reporter.value).subscribe(
      () => {
        this.reporter.reset();
        formDirective.resetForm();
        this.submitted = false;
        this.snackbarService.openSnackBar(
          'Feedback is sent!',
          'verified',
          2000
        );
      },
      (err) => {
        this.snackbarService.openSnackBar(err.error.message, 'close', 2000);
      }
    );
  }
}
