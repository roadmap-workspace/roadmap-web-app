import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { FeaturesComponent } from './components/features/features.component';
import { LandingComponent } from './landing.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
        data: {
          title: 'Roadmap - Home',
        },
      },
      {
        path: 'about',
        component: AboutComponent,
        data: {
          title: 'Roadmap - About',
        },
      },
      {
        path: 'features',
        component: FeaturesComponent,
        data: {
          title: 'Roadmap - Features',
        },
      },
      {
        path: 'contact-us',
        component: ContactUsComponent,
        data: {
          title: 'Roadmap - Contact Us',
        },
      },
      {
        path: '',
        redirectTo: 'home',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LandingRoutingModule {}
