import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { SharedModule } from '../@shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { TuiModule } from '../@tui/tui.module';
import { LandingComponent } from './landing.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { FeaturesComponent } from './components/features/features.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { MaterialModule } from '../@material/material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    LandingComponent,
    HomeComponent,
    AboutComponent,
    FeaturesComponent,
    ContactUsComponent,
  ],
  imports: [
    CommonModule,
    LandingRoutingModule,
    SharedModule,
    IonicModule,
    TuiModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
})
export class LandingModule {}
