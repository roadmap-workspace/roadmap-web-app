import { Feature } from '../models/feature.interface';

export const features: Feature[] = [
  {
    title: 'landing.features.card1title',
    description: 'landing.features.card1description',
    icon: 'book',
  },
  {
    title: 'landing.features.card2title',
    description: 'landing.features.card2description',
    icon: 'bar-chart',
  },
  {
    title: 'landing.features.card3title',
    description: 'landing.features.card3description',
    icon: 'pulse',
  },
];
