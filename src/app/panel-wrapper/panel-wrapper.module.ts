import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PanelWrapperRoutingModule } from './panel-wrapper-routing.module';
import { PanelWrapperComponent } from './panel-wrapper.component';
import { IonicModule } from '@ionic/angular';
import { MaterialModule } from '../@material/material.module';
import { SharedModule } from '../@shared/shared.module';

@NgModule({
  declarations: [PanelWrapperComponent],
  imports: [
    CommonModule,
    PanelWrapperRoutingModule,
    IonicModule,
    MaterialModule,
    SharedModule,
  ],
})
export class PanelWrapperModule {}
