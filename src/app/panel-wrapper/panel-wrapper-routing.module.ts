import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PanelWrapperComponent } from './panel-wrapper.component';

const routes: Routes = [
  {
    path: '',
    component: PanelWrapperComponent,
    children: [
      /* {
        path: 'profile',
        loadChildren: () =>
          import('../profile/profile.module').then((m) => m.ProfileModule),
      }, */
      {
        path: 'dashboard',
        loadChildren: () =>
          import('../dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'departments',
        loadChildren: () =>
          import('../department/department.module').then(
            (m) => m.DepartmentModule
          ),
      },
      {
        path: 'roadmaps',
        loadChildren: () =>
          import('../roadmap/roadmap.module').then((m) => m.RoadmapModule),
      },
      {
        path: 'roadmaps/questions-bank',
        loadChildren: () =>
          import('../questions-bank/questions-bank.module').then(
            (m) => m.QuestionsBankModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanelWrapperRoutingModule {}
