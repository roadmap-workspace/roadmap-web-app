import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Link } from '../../models/link.interface';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent implements OnInit {
  @Input() crumbs: Link[];
  constructor() {
    this.crumbs = [];
  }

  ngOnInit() {}
}
