import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Subscription } from 'rxjs';
import { ScreenService } from 'src/app/@core/services/screen/screen.service';

@Component({
  selector: 'app-company-sidenav',
  templateUrl: './company-sidenav.component.html',
  styleUrls: ['./company-sidenav.component.scss'],
})
export class CompanySidenavComponent implements AfterViewInit, OnDestroy {
  @ViewChild('sidenav') sidenave: MatSidenav;
  screenSubscription: Subscription;
  constructor(private screenService: ScreenService) {}

  ngAfterViewInit() {
    this.screenSubscription = this.screenService.isBelowSm().subscribe(() => {
      this.sidenave.close();
    });
  }

  ngOnDestroy(): void {
    this.screenSubscription.unsubscribe();
  }
}
