import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { ScreenService } from 'src/app/@core/services/screen/screen.service';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { Company } from 'src/app/auth/models/company.interface';
import { companyLinks } from '../../constants/company-links.const';
import { Link } from '../../models/link.interface';

@Component({
  selector: 'app-company-navbar',
  templateUrl: './company-navbar.component.html',
  styleUrls: ['./company-navbar.component.scss'],
})
export class CompanyNavbarComponent implements OnInit, OnDestroy {
  @Output() public sidenavToggle = new EventEmitter();
  screenSubscription: Subscription;
  isBelowMd: boolean;
  companyLinks: Link[];
  company: Company;
  constructor(
    private screenService: ScreenService,
    private storageService: StorageService
  ) {
    this.isBelowMd = false;
    this.companyLinks = companyLinks;
    this.company = this.storageService.getLocalObject('company');
  }

  ngOnInit() {
    this.screenSubscription = this.screenService
      .isBelowMd()
      .subscribe((isBelowMd) => {
        this.isBelowMd = isBelowMd.matches;
      });
  }

  toggleSidenav() {
    this.sidenavToggle.emit();
  }

  ngOnDestroy(): void {
    this.screenSubscription.unsubscribe();
  }
}
