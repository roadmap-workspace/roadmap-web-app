import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { Company } from 'src/app/auth/models/company.interface';
import { AuthService } from 'src/app/auth/services/auth.service';
import { companyLinks } from '../../constants/company-links.const';
import { Link } from '../../models/link.interface';

@Component({
  selector: 'app-company-sidenav-list',
  templateUrl: './company-sidenav-list.component.html',
  styleUrls: ['./company-sidenav-list.component.scss'],
})
export class CompanySidenavListComponent implements OnInit {
  @Output() sidenavClose = new EventEmitter();
  companyLinks: Link[];
  company: Company;
  constructor(private storageService: StorageService) {
    this.companyLinks = companyLinks;
    this.company = this.storageService.getLocalObject('company');
  }

  ngOnInit() {}
  public emitSidenavClose() {
    this.sidenavClose.emit();
  }
}
