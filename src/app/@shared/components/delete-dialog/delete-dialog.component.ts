import { Direction } from '@angular/cdk/bidi';
import { Component, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslationService } from 'src/app/@core/services/translation/translation.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
})
export class DeleteDialogComponent implements OnDestroy {
  @Input() title: string;
  @Input() message: string;
  @Input() actionMessage: string;
  direction: Direction;
  isConfirmed: boolean;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: string,
    private dialogRef: MatDialogRef<DeleteDialogComponent>,
    private translationService: TranslationService
  ) {
    this.direction = 'ltr';
    this.title = '';
    this.message = '';
    this.actionMessage = '';
    this.isConfirmed = false;
    this.subscribeLayoutDirection();
  }
  confirm(): void {
    this.isConfirmed = true;
    this.dialogRef.close({ decision: true });
  }

  decline(): void {
    this.dialogRef.close({ decision: false });
  }

  subscribeLayoutDirection(): void {
    this.translationService.direction.subscribe((dir) => {
      this.direction = dir;
    });
  }

  ngOnDestroy(): void {
    if (!this.isConfirmed) {
      this.dialogRef.close({ decision: false });
    }
  }
}
