import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { TranslationService } from 'src/app/@core/services/translation/translation.service';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnInit {
  logo: string;
  constructor(
    private storageService: StorageService,
    private translationService: TranslationService,
    private authService: AuthService
  ) {
    this.logo = this.storageService.getLocalObject('company').logo;
  }

  ngOnInit() {}

  switchLanguage() {
    const lang = this.storageService.getLocalItem('lang');
    if (lang === 'ar') {
      this.translationService.selectEnglish();
    } else {
      this.translationService.selectArabic();
    }
  }

  signout() {
    this.authService.signout();
    this.authService.removeCredentials();
  }
}
