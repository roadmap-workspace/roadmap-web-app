import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { TranslationService } from 'src/app/@core/services/translation/translation.service';
import { AuthService } from 'src/app/auth/services/auth.service';
import { links } from '../../constants/links.const';
import { Link } from '../../models/link.interface';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {
  links: Link[];
  constructor(
    private storageService: StorageService,
    private translationService: TranslationService,
    private authService: AuthService,
    private router: Router
  ) {
    this.links = links;
  }

  ngOnInit() {}

  switchLanguage() {
    const lang = this.storageService.getLocalItem('lang');
    if (lang === 'ar') {
      this.translationService.selectEnglish();
    } else {
      this.translationService.selectArabic();
    }
  }
  navigatePreference() {
    if (this.authService.isAuth()) {
      this.router.navigate(['/panel/dashboard']);
    } else {
      this.router.navigate(['/auth/login']);
    }
  }
}
