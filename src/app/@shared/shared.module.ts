import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../@material/material.module';
import { TuiModule } from '../@tui/tui.module';
import { HomeButtonComponent } from './components/home-button/home-button.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { CompanyNavbarComponent } from './components/company-navbar/company-navbar.component';
import { CompanySidenavComponent } from './components/company-sidenav/company-sidenav.component';
import { CompanySidenavListComponent } from './components/company-sidenav-list/company-sidenav-list.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { DeleteDialogComponent } from './components/delete-dialog/delete-dialog.component';

const shared = [
  NavbarComponent,
  SidenavComponent,
  HomeButtonComponent,
  SnackBarComponent,
  CompanyNavbarComponent,
  CompanySidenavComponent,
  CompanySidenavListComponent,
  AvatarComponent,
  BreadcrumbComponent,
  DeleteDialogComponent,
];
@NgModule({
  declarations: [...shared],
  imports: [
    CommonModule,
    IonicModule,
    MaterialModule,
    TuiModule,
    RouterModule,
    TranslateModule,
    NgxSpinnerModule,
    FlexLayoutModule,
    NgbModule, //TODO: Tree shake here after investigation
  ],
  exports: [...shared, TranslateModule, NgxSpinnerModule, FlexLayoutModule],
})
export class SharedModule {}
