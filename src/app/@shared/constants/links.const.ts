import { Link } from '../models/link.interface';

export const links: Link[] = [
  { label: 'navbar.home', route: '/home', icon: 'home' },
  { label: 'navbar.about', route: '/about', icon: 'book' },
  { label: 'navbar.features', route: '/features', icon: 'sparkles' },
  { label: 'navbar.contactUs', route: '/contact-us', icon: 'chatbubbles' },
];
