import { Link } from '../models/link.interface';

export const companyLinks: Link[] = [
  {
    label: 'navbar.dashboard',
    route: '/panel/dashboard',
    icon: 'space_dashboard',
  },
  {
    label: 'navbar.departments',
    route: '/panel/departments',
    icon: 'apartment',
  },
  {
    label: 'navbar.roadmaps',
    route: '/panel/roadmaps',
    icon: 'signpost',
  },
];
