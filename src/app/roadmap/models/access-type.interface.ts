export interface AccessType {
  name: string;
  value: string;
}
