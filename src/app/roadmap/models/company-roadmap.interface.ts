import { Department } from 'src/app/department/models/department.interface';

export interface CompanyRoadmap {
  _id: string;
  departmentId: string;
  companyId: string;
  title: string;
  description: string;
  published: boolean;
  createdAt: Date;
  updatedAt: Date;
  department?: Department;
}
