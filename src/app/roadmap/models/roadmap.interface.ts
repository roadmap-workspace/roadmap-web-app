export interface Roadmap {
  _id: string;
  companyId: string;
  createdAt: Date;
  updatedAt: Date;
  title: string;
  description: string;
  published: boolean;
}
