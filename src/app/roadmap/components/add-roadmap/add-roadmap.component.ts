import { Direction } from '@angular/cdk/bidi';
import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { TranslationService } from 'src/app/@core/services/translation/translation.service';
import { Department } from 'src/app/department/models/department.interface';
import { DepartmentService } from 'src/app/department/services/department.service';
import { RoadmapService } from '../../services/roadmap.service';

@Component({
  selector: 'app-add-roadmap',
  templateUrl: './add-roadmap.component.html',
  styleUrls: ['./add-roadmap.component.scss'],
})
export class AddRoadmapComponent implements OnInit, OnDestroy {
  @Output() isAdded: EventEmitter<boolean>;
  direction: Direction;
  departments: Department[];
  roadmap: FormGroup;
  submitted = false;
  isConfirmed = false;
  constructor(
    private translationService: TranslationService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AddRoadmapComponent>,
    private roadmapService: RoadmapService,
    private departmentService: DepartmentService
  ) {
    this.isAdded = new EventEmitter<boolean>();
    this.direction = 'ltr';
    this.departments = [];
    this.subscribeLayoutDirection();
    this.roadmap = this.initRoadmapForm();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.roadmap.controls;
  }

  ngOnInit() {
    this.getDepartments();
  }

  subscribeLayoutDirection(): void {
    this.translationService.direction.subscribe((dir) => {
      this.direction = dir;
    });
  }

  initRoadmapForm() {
    return this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      departmentId: ['', Validators.required],
    });
  }

  getDepartments() {
    this.departmentService.getDepartments().subscribe((departments) => {
      this.departments = departments;
    });
  }

  submit(): void {
    this.submitted = true;
    if (this.roadmap.invalid) {
      return;
    }
    this.addRoadmap();
  }

  addRoadmap() {
    this.roadmapService
      .createRoadmap(this.roadmap.value.departmentId, this.roadmap.value)
      .subscribe(
        () => {
          this.isConfirmed = true;
          this.dialogRef.close({ decision: true });
        },
        () => {
          this.dialogRef.close();
        }
      );
  }

  cancel() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    if (!this.isConfirmed) {
      this.dialogRef.close({ decision: false });
    }
  }
}
