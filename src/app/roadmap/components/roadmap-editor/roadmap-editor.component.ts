import { MatFabMenu } from '@angular-material-extensions/fab-menu';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Node } from 'src/app/@graph/models/node';
import { NodeService } from '../../services/node.service';

@Component({
  selector: 'app-roadmap-editor',
  templateUrl: './roadmap-editor.component.html',
  styleUrls: ['./roadmap-editor.component.scss'],
})
export class RoadmapEditorComponent implements OnInit {
  nodes: Node[];
  nodeSettings: Node;
  isLoaded = false;
  tools: MatFabMenu[];
  tool: string | number;
  trigger = true;
  isOpen = false;
  constructor(
    private cdref: ChangeDetectorRef,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private nodeService: NodeService
  ) {
    this.nodes = [];
    this.tools = [
      {
        id: 1,
        imgUrl: 'assets/icon/node.png',
        tooltip: this.translate.instant('tooltip.node'),
        tooltipPosition: 'left',
      },
      /* {
        id: 2,
        imgUrl: 'assets/icon/connector.png',
        tooltip: this.translate.instant('tooltip.connector'),
        tooltipPosition: 'left',
      }, */
      {
        id: 3,
        imgUrl: 'assets/icon/hand.png',
        tooltip: this.translate.instant('tooltip.grip'),
        tooltipPosition: 'left',
      },
      {
        id: 4,
        imgUrl: 'assets/icon/cursor.png',
        tooltip: this.translate.instant('tooltip.default'),
        tooltipPosition: 'left',
      },
      /* {
        id: 5,
        imgUrl: 'assets/icon/question.png',
        tooltip: this.translate.instant('tooltip.quickTour'),
        tooltipPosition: 'left',
      }, */
    ];
  }

  ngOnInit() {
    this.getNodes();
  }

  getNodes() {
    this.nodeService.getNodes(this.getID()).subscribe((nodes) => {
      this.nodes = nodes;
      this.isLoaded = true;
    });
  }

  getID() {
    return this.activatedRoute.snapshot.paramMap.get('rdId');
  }

  chooseTool(tool: string | number) {
    this.tool = tool;
  }

  notify(node: Node) {
    this.isOpen = true;
    this.nodeSettings = node;
  }

  updateNodes(nodes: any) {
    this.trigger = false;
    this.isOpen = false;
    this.nodes = nodes;
    setTimeout(() => {
      this.trigger = true;
    }, 0);
  }

  toggle() {
    this.isOpen = false;
    this.cdref.detectChanges();
  }
}
