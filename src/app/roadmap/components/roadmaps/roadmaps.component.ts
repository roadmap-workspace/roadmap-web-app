import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { fade } from 'src/app/@shared/animations/fade';
import { CompanyRoadmap } from '../../models/company-roadmap.interface';
import { RoadmapService } from '../../services/roadmap.service';
import { AddRoadmapComponent } from '../add-roadmap/add-roadmap.component';

@Component({
  selector: 'app-roadmaps',
  templateUrl: './roadmaps.component.html',
  styleUrls: ['./roadmaps.component.scss'],
  animations: [fade],
})
export class RoadmapsComponent implements OnInit {
  roadmaps: CompanyRoadmap[];
  isLoaded: boolean;
  constructor(
    private dialog: MatDialog,
    private roadmapService: RoadmapService
  ) {
    this.roadmaps = [];
    this.isLoaded = false;
  }

  ngOnInit() {
    this.getCompanyRoadmaps();
  }

  getCompanyRoadmaps() {
    this.roadmapService.getCompanyRoadmaps().subscribe((roadmaps) => {
      this.roadmaps = roadmaps;
      this.isLoaded = true;
    });
  }

  openRoadmapDialog() {
    const dialogRef = this.dialog.open(AddRoadmapComponent, {
      width: '75%',
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.decision) {
        this.getCompanyRoadmaps();
      }
    });
  }
}
