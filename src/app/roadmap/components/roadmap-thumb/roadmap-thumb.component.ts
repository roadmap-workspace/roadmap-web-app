import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Node } from 'src/app/@graph/models/node';
import { fade } from 'src/app/@shared/animations/fade';
import { NodeService } from '../../services/node.service';

@Component({
  selector: 'app-roadmap-thumb',
  templateUrl: './roadmap-thumb.component.html',
  styleUrls: ['./roadmap-thumb.component.scss'],
  animations: [fade],
})
export class RoadmapThumbComponent implements OnInit {
  nodes: Node[];
  isLoaded = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private nodeService: NodeService
  ) {
    this.nodes = [];
  }

  ngOnInit() {
    this.getNodes();
  }

  getNodes() {
    this.nodeService.getNodes(this.getID()).subscribe((nodes) => {
      this.nodes = nodes;
      this.isLoaded = true;
    });
  }

  getID() {
    return this.activatedRoute.snapshot.paramMap.get('rdId');
  }
}
