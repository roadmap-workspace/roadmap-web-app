import { Component, OnInit, ViewChild } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TuiHostedDropdownComponent } from '@taiga-ui/core';
import { StorageService } from 'src/app/@core/services/storage/storage.service';
import { SnackbarService } from 'src/app/@material/services/snackbar.service';
import { fade } from 'src/app/@shared/animations/fade';
import { DeleteDialogComponent } from 'src/app/@shared/components/delete-dialog/delete-dialog.component';
import { Link } from 'src/app/@shared/models/link.interface';
import { CompanyRoadmap } from '../../models/company-roadmap.interface';
import { RoadmapService } from '../../services/roadmap.service';

@Component({
  selector: 'app-update-roadmap',
  templateUrl: './update-roadmap.component.html',
  styleUrls: ['./update-roadmap.component.scss'],
  animations: [fade],
})
export class UpdateRoadmapComponent implements OnInit {
  @ViewChild(TuiHostedDropdownComponent) component?: TuiHostedDropdownComponent;
  roadmap: FormGroup;
  crumbs: Link[];
  rdId: string;
  deptId: string;
  submitted = false;
  isLoaded = false;
  isOpen = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private roadmapService: RoadmapService,
    private snackBarService: SnackbarService,
    private dialog: MatDialog,
    private storageService: StorageService
  ) {
    this.crumbs = this.initCrumbs();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.roadmap.controls;
  }

  ngOnInit() {
    this.getIDs();
  }

  initCrumbs(): Link[] {
    return [
      { label: 'crumb.roadmaps', route: '/panel/roadmaps' },
      {
        label: 'crumb.updateRoadmap',
        route: `/panel/roadmap/update-roadmap/${this.deptId}/${this.rdId}`,
      },
    ];
  }

  getIDs() {
    this.deptId = this.activatedRoute.snapshot.paramMap.get('deptId');
    this.rdId = this.activatedRoute.snapshot.paramMap.get('rdId');
    if (this.deptId && this.rdId) {
      this.storageService.setLocalItem('deptId', this.deptId);
      this.getRoadmap(this.deptId, this.rdId);
    }
  }

  getRoadmap(deptId: string, rdId: string) {
    this.roadmapService.getRoadmap(deptId, rdId).subscribe((roadmap) => {
      this.roadmap = this.initRoadmap(roadmap);
      this.isLoaded = true;
    });
  }

  initRoadmap(roadmap: CompanyRoadmap) {
    return this.fb.group({
      _id: [roadmap._id, Validators.required],
      companyId: [roadmap.companyId, Validators.required],
      departmentId: [roadmap.departmentId, Validators.required],
      createdAt: [roadmap.createdAt, Validators.required],
      updatedAt: [roadmap.updatedAt, Validators.required],
      published: [roadmap.published, Validators.required],
      title: [roadmap.title, Validators.required],
      description: [roadmap.description, Validators.required],
    });
  }

  submit(): void {
    this.submitted = true;
    if (this.roadmap.invalid) {
      return;
    }
    this.updateRoadmap();
  }

  updateRoadmap() {
    this.roadmapService
      .updateRoadmap(this.deptId, this.roadmap.value)
      .subscribe(
        () => {
          this.router.navigate(['/panel/roadmaps']);
        },
        (err) => {
          console.error(err);
        }
      );
  }

  publish() {
    this.roadmapService.publishRoadmap(this.deptId, this.rdId).subscribe(
      () => {
        this.f['published'].setValue(true);
        this.snackBarService.openSnackBar(
          'Roadmap published successfully!',
          'celebration',
          2000
        );
      },
      (err) => {
        console.error(err);
      }
    );
  }

  unPublish() {
    this.roadmapService.unPublishRoadmap(this.deptId, this.rdId).subscribe(
      () => {
        this.f['published'].setValue(false);
        this.snackBarService.openSnackBar(
          'Roadmap unpublished successfully!',
          'fence',
          2000
        );
      },
      (err) => {
        console.error(err);
      }
    );
  }

  openDialog() {
    let dialogRef = this.dialog.open(DeleteDialogComponent);
    let instance = dialogRef.componentInstance;
    instance.title = 'dialog.caution';
    instance.message = 'dialog.removeRoadmap';
    instance.actionMessage = 'btn.deleteRoadmap';
    dialogRef.afterClosed().subscribe((result) => {
      if (result.decision) {
        this.roadmapService
          .deleteRoadmap(this.deptId, this.rdId)
          .subscribe(() => {
            this.router.navigate(['/panel/roadmaps']);
          });
      }
    });
  }
}
