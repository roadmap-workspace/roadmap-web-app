import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { fade } from 'src/app/@shared/animations/fade';
import { AddRoadmapComponent } from '../add-roadmap/add-roadmap.component';

@Component({
  selector: 'app-empty-roadmap',
  templateUrl: './empty-roadmap.component.html',
  styleUrls: ['./empty-roadmap.component.scss'],
  animations: [fade],
})
export class EmptyRoadmapComponent implements OnInit {
  @Output() hasRoadmap: EventEmitter<boolean>;
  constructor(private dialog: MatDialog) {
    this.hasRoadmap = new EventEmitter<boolean>();
  }

  ngOnInit() {}

  openRoadmapDialog() {
    const dialogRef = this.dialog.open(AddRoadmapComponent, {
      width: '75%',
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.decision) {
        this.hasRoadmap.emit(true);
      }
    });
  }
}
