import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoadmapEditorComponent } from './components/roadmap-editor/roadmap-editor.component';
import { RoadmapsComponent } from './components/roadmaps/roadmaps.component';
import { UpdateRoadmapComponent } from './components/update-roadmap/update-roadmap.component';

const routes: Routes = [
  { path: '', component: RoadmapsComponent },
  { path: 'update-roadmap/:deptId/:rdId', component: UpdateRoadmapComponent },
  { path: ':rdId/editor', component: RoadmapEditorComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoadmapRoutingModule {}
