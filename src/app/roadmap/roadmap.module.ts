import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoadmapRoutingModule } from './roadmap-routing.module';
import { RoadmapsComponent } from './components/roadmaps/roadmaps.component';
import { SharedModule } from '../@shared/shared.module';
import { EmptyRoadmapComponent } from './components/empty-roadmap/empty-roadmap.component';
import { TuiModule } from '../@tui/tui.module';
import { AddRoadmapComponent } from './components/add-roadmap/add-roadmap.component';
import { MaterialModule } from '../@material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { UpdateRoadmapComponent } from './components/update-roadmap/update-roadmap.component';
import { RoadmapThumbComponent } from './components/roadmap-thumb/roadmap-thumb.component';
import { GraphModule } from '../@graph/graph.module';
import { RoadmapEditorComponent } from './components/roadmap-editor/roadmap-editor.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    RoadmapsComponent,
    EmptyRoadmapComponent,
    AddRoadmapComponent,
    UpdateRoadmapComponent,
    RoadmapThumbComponent,
    RoadmapEditorComponent
  ],
  imports: [
    CommonModule,
    RoadmapRoutingModule,
    SharedModule,
    MaterialModule,
    TuiModule,
    ReactiveFormsModule,
    GraphModule,
    RouterModule
  ],
})
export class RoadmapModule {}
