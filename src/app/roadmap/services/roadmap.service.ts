import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { endpoints } from 'src/app/@api/constants/endpoints.const';
import { environment } from 'src/environments/environment';
import { CompanyRoadmap } from '../models/company-roadmap.interface';
import { Roadmap } from '../models/roadmap.interface';

@Injectable({
  providedIn: 'root',
})
export class RoadmapService {
  endpoint: string;
  constructor(private http: HttpClient) {
    this.endpoint = `${environment.baseURL}/${endpoints.departments}`;
  }

  createRoadmap(departmentId: string, roadmap: Roadmap): Observable<Roadmap> {
    return this.http.post<Roadmap>(
      `${this.endpoint}/${departmentId}/${endpoints.roadmaps}`,
      roadmap
    );
  }

  getRoadmaps(departmentId: string): Observable<Roadmap[]> {
    return this.http.get<Roadmap[]>(
      `${this.endpoint}/${departmentId}/${endpoints.roadmaps}`
    );
  }

  getRoadmap(departmentId: string, id: string): Observable<CompanyRoadmap> {
    return this.http.get<CompanyRoadmap>(
      `${this.endpoint}/${departmentId}/${endpoints.roadmaps}/${id}`
    );
  }

  updateRoadmap(departmentId: string, roadmap: Roadmap): Observable<Roadmap> {
    return this.http.patch<Roadmap>(
      `${this.endpoint}/${departmentId}/${endpoints.roadmaps}/${roadmap._id}`,
      roadmap
    );
  }

  deleteRoadmap(departmentId: string, id: string): Observable<Roadmap> {
    return this.http.delete<Roadmap>(
      `${this.endpoint}/${departmentId}/${endpoints.roadmaps}/${id}`
    );
  }

  getCompanyRoadmaps(): Observable<CompanyRoadmap[]> {
    return this.http.get<CompanyRoadmap[]>(
      `${environment.baseURL}/${endpoints.roadmaps}`
    );
  }

  publishRoadmap(departmentId: string, roadmapId: string): Observable<any> {
    return this.http.patch<any>(
      `${this.endpoint}/${departmentId}/${endpoints.roadmaps}/${roadmapId}/publish`,
      {}
    );
  }

  unPublishRoadmap(departmentId: string, roadmapId: string): Observable<any> {
    return this.http.patch<any>(
      `${this.endpoint}/${departmentId}/${endpoints.roadmaps}/${roadmapId}/unpublish`,
      {}
    );
  }
}
