import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { endpoints } from 'src/app/@api/constants/endpoints.const';
import { Node } from 'src/app/@graph/models/node';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class NodeService {
  endpoint: string;
  constructor(private http: HttpClient) {
    this.endpoint = `${environment.baseURL}/${endpoints.roadmaps}`;
  }

  createNode(roadmapId: string, node: Node): Observable<Node[]> {
    return this.http.post<Node[]>(
      `${this.endpoint}/${roadmapId}/${endpoints.nodes}`,
      node
    );
  }

  getNodes(roadmapId: string): Observable<Node[]> {
    return this.http.get<Node[]>(
      `${this.endpoint}/${roadmapId}/${endpoints.nodes}`
    );
  }

  getNode(roadmapId: string, id: string): Observable<Node> {
    return this.http.get<Node>(
      `${this.endpoint}/${roadmapId}/${endpoints.nodes}/${id}`
    );
  }

  updateNode(roadmapId: string, node: Node): Observable<Node[]> {
    return this.http.patch<Node[]>(
      `${this.endpoint}/${roadmapId}/${endpoints.nodes}/${node._id}`,
      node
    );
  }

  deleteNode(roadmapId: string, id: string): Observable<Node[]> {
    return this.http.delete<Node[]>(
      `${this.endpoint}/${roadmapId}/${endpoints.nodes}/${id}`
    );
  }

  postCycleNode(roadmapId: string, node: Node): Observable<any> {
    return this.http.post<Node[]>(
      `${this.endpoint}/${roadmapId}/${endpoints.nodes}/cycleState`,
      [node]
    );
  }
}
