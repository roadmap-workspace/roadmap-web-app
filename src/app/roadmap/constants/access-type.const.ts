import { AccessType } from '../models/access-type.interface';

export const accessTypes: AccessType[] = [
  { name: 'At Least One', value: 'atLeastOne' },
  { name: 'Optional', value: 'optional' },
  { name: 'Required', value: 'required' },
];
